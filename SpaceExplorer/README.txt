Table of Contents:
- Folder Structure
- Running the .jar file of our game
- How to play the game
- How to import our project into Eclipse
- How to run our project in Eclipse


Folder Structure:
    SpaceExplorer ---> contains all of the project files
        bin --> contains the files that Eclipse compiles
        doc --> contains the javadoc of our project
        uml-testing --> contains the uml diagrams of our game and gui classes, test coverage and use case diagram
        src --> contains the source code of the project
        coverage.PNG --> a screen capture showing the coverage of our unit tests
        jth135_amu112_SpaceExplorer.jar --> a runnable jar file of our game
        README.txt --> this document explaining everything

Running the .jar file of our game steps:
    1. Ensure Java 11 is installed on the machine
    2. Open a terminal and navigate to the SpaceExplorer folder
    3. run the command:
        java -jar jth135_amu112_SpaceExplorer.jar

Importing our project into Eclipse:
    1. File -> New -> Java Project
    2. Uncheck use default location
    3. Click browse and select the SpaceExplorer folder extracted from the zip
    4. Project name should automatically change to be SpaceExplorer
    5. Click next and uncheck "Create module-info.java"
    6. Click finish

Running our project in Eclipse:
    To run the command line application:
        1. Navigate to the src -> commandLine -> CommandLine.java class
        2. Click run and if prompted select java application
        	or if the above fails
        2. Click Run -> Run as -> Java Application 
    
    To run the gui application:
        1. Navigate to the src -> gui -> WindowManager.java class
        2. Click run and if prompted select java application
        	or if the above fails
        2. Click Run -> Run as -> Java Application

    To run the Junit tests
        1. Navigate to the src -> tests -> AllTests.java class
        2. Click Run -> Run as -> Junit Test      
