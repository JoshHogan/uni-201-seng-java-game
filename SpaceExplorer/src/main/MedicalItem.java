package main;

/**
 * MedicalItem class contains information surrounding the three items that
 * effect the players health, and inherits information from the Item class, to
 * be displayed in the space shop
 */

public class MedicalItem extends Item {
	public enum MedicalItemType {
		SpacePlagueCure, HealthBoost, FullHeal
	}

	private MedicalItemType type;

	/**
	 * Makes a new medical item with the type as provided
	 * 
	 * @param type the type of the medical item, e.g. HealthBoost
	 */
	public MedicalItem(MedicalItemType type) {
		this.type = type;
	}

	/**
	 * Returns true if the current medical item is a cure for space plague
	 * 
	 * @return is this medical item a cure for space plague
	 */
	public boolean isSpacePlagueCure() {
		return type == MedicalItemType.SpacePlagueCure;
	}

	/**
	 * Returns the amount a crew member will be healed by using this item
	 * 
	 * @return the amount a crew member will be healed
	 */
	public int healAmount() {
		switch (type) {
		case SpacePlagueCure:
			return 10;
		case FullHeal:
			return 100;
		default: // Health boost
			return 30;
		}
	}

	@Override
	public int price() {
		switch (type) {
		case SpacePlagueCure:
			return 100;
		case FullHeal:
			return 60;
		default: // Health Boost
			return 25;
		}
	}

	@Override
	public ItemType getItemType() {
		return ItemType.Medical;
	}

	@Override
	public String toString() {
		switch (type) {
		case FullHeal:
			return "Full Health Potion";
		case HealthBoost:
			return "Health Booster 20%";
		default: // Space Plague cure
			return "Space Plague Cure";
		}
	}

	@Override
	public Item clone() {
		return new MedicalItem(type);
	}
}
