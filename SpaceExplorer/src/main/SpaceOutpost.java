package main;

import main.MedicalItem.MedicalItemType;
import main.FoodItem.FoodItemType;
import java.util.ArrayList;
import java.util.Collections;

/**
 * SpaceOutpost contains information about the in game shop, as well as buying
 * items and storing them
 */

public class SpaceOutpost {
	/**
	 * The parent game that this outpost is a part of
	 */
	private Game parentGame;
	/**
	 * The inventory list of all of the items this outpost can sell
	 */
	private ArrayList<Item> inventory;

	/**
	 * Creates a new space outpost object which automatically adds one of every item
	 * to its inventory
	 * 
	 * @param parentGame the game that this outpost is a part of
	 */
	public SpaceOutpost(Game parentGame) {
		this.parentGame = parentGame;
		inventory = new ArrayList<Item>();

		// Add all of the medical and food items to the inventory
		for (MedicalItemType type : MedicalItemType.values()) {
			inventory.add(new MedicalItem(type));
		}
		for (FoodItemType type : FoodItemType.values()) {
			inventory.add(new FoodItem(type));
		}
		Collections.sort(inventory);
	}

	/**
	 * This method first checks that the crew has enough money to buy the selected
	 * item. If not it returns false. If they can afford it then it adds the item to
	 * the crew's inventory and minuses the price from the crews bank balance
	 * 
	 * @param choice an integer corresponding to the chosen item in the inventory
	 * @return true if the item was bought, false if the user couldn't afford it
	 */
	public boolean buy(int choice) {
		Item chosenItem = inventory.get(choice);
		return buy(chosenItem);
	}

	/**
	 * This method first checks that the crew has enough money to buy the selected
	 * item. If not it returns false. If they can afford it then it adds the item to
	 * the crew's inventory and minuses the price from the crews bank balance
	 * 
	 * @param item the item that the crew is trying to buy
	 * @return true if the item was bought, false if the user couldn't afford it
	 */
	public boolean buy(Item item) {
		if (item.price() <= parentGame.crew.bankAccount) {
			parentGame.crew.bankAccount -= item.price();
			// Clone the item so a fresh one is added to the inventory
			parentGame.crew.addItemToInventory(item.clone());
			return true;
		}
		return false;
	}

	/**
	 * Returns the list of all items in the inventory
	 * 
	 * @return The list containing all items in the inventory
	 */
	public ArrayList<Item> getInventory() {
		return inventory;
	}

}
