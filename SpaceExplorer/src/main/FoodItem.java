package main;

/**
 * FoodItem contains the information surrounding the food which can be found and
 * bought, amount of hunger they're worth, as well as their price
 */

public class FoodItem extends Item {
	public enum FoodItemType {
		Lettuce, Chocolate, Bacon, EnergyDrink, Sandwhich, ButterChicken
	}

	/**
	 * The type of food-item that this item is
	 */
	private FoodItemType type;

	/**
	 * Makes a new food-item with the specified type
	 * 
	 * @param type the type of food-item
	 */
	public FoodItem(FoodItemType type) {
		this.type = type;
	}

	@Override
	public ItemType getItemType() {
		return ItemType.Food;
	}

	/**
	 * Returns the nutritional value of this food item
	 * 
	 * @return the nutritional value
	 */
	public int nutritionalValue() {
		switch (type) {
		case Lettuce:
			return 2;
		case Chocolate:
			return 6;
		case Bacon:
			return 16;
		case EnergyDrink:
			return 24;
		case Sandwhich:
			return 30;
		default: // Butter Chicken
			return 50;
		}
	}

	/**
	 * Returns the name as a string of what this food-item is
	 * 
	 * @return a string of the name of this food e.g. "Bacon"
	 */
	private String name() {
		switch (type) {
		case Bacon:
			return "Bacon";
		case ButterChicken:
			return "Butter Chicken";
		case Chocolate:
			return "Chocolate";
		case EnergyDrink:
			return "Energy Drink";
		case Lettuce:
			return "Lettuce";
		default: // Sandwhich
			return "Sandwhich";
		}
	}

	@Override
	public int price() {
		return nutritionalValue() / 2 + 10;
	}

	@Override
	public String toString() {
		return name() + " (" + nutritionalValue() + ")";
	}

	@Override
	public Item clone() {
		return new FoodItem(type);
	}
}
