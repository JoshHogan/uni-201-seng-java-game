package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

/**
 * Crew class stores a list of CrewMembers, with their inventory and bank
 * account
 */

public class Crew {
	/**
	 * The parent game that this crew is a part of This allows the crew to call
	 * methods that are from the game class
	 */
	public Game parentGame;
	/**
	 * The list of crew members that make up this crew
	 */
	private ArrayList<CrewMember> crewMembers;

	/**
	 * The list of items that the crew has
	 */
	private ArrayList<Item> inventory;

	/**
	 * The amount of money that the crew has
	 */
	public int bankAccount;

	/**
	 * Creates a crew object that holds a list of all crew members and the shared
	 * inventory of items
	 * 
	 * @param parentGame the game that this crew is a part of
	 */
	public Crew(Game parentGame) {
		this.parentGame = parentGame;
		crewMembers = new ArrayList<CrewMember>();
		inventory = new ArrayList<Item>();
		bankAccount = 500; // amount of money to start with
	}

	/**
	 * Adds a new member to the list of crew members this should only be used at the
	 * start of the game
	 * 
	 * @param memberToAdd the member to add to the crew
	 */
	public void addMember(CrewMember memberToAdd) {
		crewMembers.add(memberToAdd);
	}

	/**
	 * Adds new item to the list of items in the inventory, then sorts the inventory
	 * 
	 * @param itemToAdd the item that is to be added
	 */
	public void addItemToInventory(Item itemToAdd) {
		inventory.add(itemToAdd);
		Collections.sort(inventory);
	}

	/**
	 * Removes an item from the inventory of the crew
	 * 
	 * @param itemToRemove the item that is to be removed
	 */
	public void removeItemFromInventory(Item itemToRemove) {
		inventory.remove(itemToRemove);
	}

	/**
	 * Returns the number of people in the crew
	 * 
	 * @return the number of people
	 */
	public int numberOfCrew() {
		return crewMembers.size();
	}

	/**
	 * Returns the list of all the items in the inventory
	 * 
	 * @return the list of items in the inventory
	 */
	public ArrayList<Item> getInventory() {
		return inventory;
	}

	/**
	 * Returns a set with only the unique items in an inventory
	 * 
	 * @return a set containing only the unique items in an inventory
	 */
	public TreeSet<Item> getDistinctItems() {
		return new TreeSet<Item>(inventory);
	}

	/**
	 * Returns a crew member that is at the given index
	 * 
	 * @param index the number of the crew member in the crew starts from 0
	 * @return the crew member at the given index
	 */
	public CrewMember getCrewMember(int index) {
		return crewMembers.get(index);
	}

	/**
	 * Returns a list containing all of the crew members in the crew
	 * 
	 * @return the list of crew members
	 */
	public ArrayList<CrewMember> getCrewMembers() {
		return crewMembers;
	}

	/**
	 * Returns a list containing all free crew members
	 * 
	 * @return a list containing all crew members with a free action
	 */
	public ArrayList<CrewMember> getFreeCrewMembers() {
		ArrayList<CrewMember> tempFreeCrewMembers = new ArrayList<CrewMember>();

		for (CrewMember crewMember : crewMembers) {
			if (crewMember.freeAction())
				tempFreeCrewMembers.add(crewMember);
		}
		return tempFreeCrewMembers;
	}

	/**
	 * Counts the number of crew members that still have an action remaining in the
	 * day
	 * 
	 * @return the number of crew members who have a free action
	 */
	public int freeCrewMembers() {
		int counter = 0;
		for (CrewMember crewMember : crewMembers) {
			if (crewMember.freeAction())
				counter++;
		}
		return counter;
	}

	/**
	 * Moves the crew onto the next day by calling the nextDay method of each crew
	 * member. This method should only be called from the game class
	 */
	public void nextDay() {
		for (CrewMember crewMember : crewMembers) {
			crewMember.nextDay();
		}
	}

}
