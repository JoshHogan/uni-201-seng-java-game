package main;

/**
 * MoneyBag class contains the moneybag random drop that can be found when
 * exploring a planet. Inherits from Item class to add found-money to bank
 * account
 */

public class MoneyBag extends Item {

	/**
	 * The amount of money that is in this money bag
	 */
	private int amount;
	/**
	 * An array which holds the possible values of a money bag
	 */
	public static int[] amounts = { 20, 50, 100, 200, 500 };

	/**
	 * Makes a new money bag item that contains the given amount of money
	 * 
	 * @param amount the amount of money that this bag will contain
	 */
	public MoneyBag(int amount) {
		this.amount = amount;
	}

	@Override
	public ItemType getItemType() {
		return ItemType.MoneyBag;
	}

	@Override
	public int price() {
		return amount;
	}

	@Override
	public String toString() {
		return "Money Bag containing: $" + amount;
	}

	@Override
	public Item clone() {
		return new MoneyBag(amount);
	}
}
