package main;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * CrewMember class contains the information surrounding each crew member
 * including name, type, health, tiredness, actions, hunger and if they have the
 * space plague
 */

public class CrewMember {
	public enum CrewMemberTypes {
		Plain, ExtraHealth, Mechanic, Explorer, Sleeper, Pilot
	}

	/**
	 * The crew that this person belongs to
	 */
	private Crew parentCrew;
	/**
	 * A number representing the type of crew member
	 */
	private CrewMemberTypes type;
	/**
	 * The user chosen name of this crew member
	 */
	private String name;
	/**
	 * The health that this crew member has, between 0-100
	 */
	public int health;
	/**
	 * How tired this crew member is, as a percentage from 0-100
	 */
	public int tiredness;
	/**
	 * How hungry this crew member is, from 0-100
	 */
	public int hunger;

	/**
	 * The number of actions that this crew member has done in one day
	 */
	private int numberOfActions;
	/**
	 * Whether this crew member has the space plague or not
	 */
	public boolean hasSpacePlague;

	/**
	 * Initializes a new CrewMember object with default values of health tiredness
	 * and hunger, the user can choose the name and type of the CrewMember
	 * 
	 * @param parentCrew the reference to the crew that this member is a part of
	 * @param name       the name of the crew member given by the user
	 * @param type       the user selected type of crew member
	 */
	public CrewMember(Crew parentCrew, String name, CrewMemberTypes type) {
		// Set global variables
		this.parentCrew = parentCrew;
		this.type = type;
		this.name = name;
		// Default values
		health = getMaxHealth();
		hasSpacePlague = false;
		tiredness = 0;
		hunger = 0;
		numberOfActions = 0;
	}

	/**
	 * Returns the name of this crew member
	 * 
	 * @return their name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the a string which contains the type of this crew member This should
	 * be used for the user interface
	 * 
	 * @return the type of this crew member in words
	 */
	public String getType() {
		switch (type) {
		case Explorer:
			return "Explorer";
		case ExtraHealth:
			return "Extra-Health";
		case Mechanic:
			return "Mechanic";
		case Pilot:
			return "Pilot";
		case Sleeper:
			return "Sleeper";
		default:
			return "Plain";
		}
	}

	/**
	 * Return the max health of this crew member, it is normally 100, but can be 150
	 * if the crew member is of the ExtraHealth type
	 * 
	 * @return the max health as a number either 100 or 150
	 */
	private int getMaxHealth() {
		if (type == CrewMemberTypes.ExtraHealth)
			return 130;
		else
			return 100;
	}

	/**
	 * Returns whether the crew member has any actions left
	 * 
	 * @return true if the user has free actions, false otherwise
	 */
	public boolean freeAction() {
		return (numberOfActions < 2);
	}

	/**
	 * Returns the number of free actions that this crew member has left
	 * 
	 * @return the number of actions completed subtracted from two
	 */
	public int getNumberOfFreeActions() {
		return 2 - numberOfActions;
	}

	/**
	 * Infects this crew member with the space plague This causes them to lose a
	 * certain amount of health each turn
	 */
	public void infect() {
		hasSpacePlague = true;
	}

	/**
	 * Method that is called every action. Makes the crew member a little bit more
	 * tired and a little bit more hungry every time they do an action
	 * 
	 * @param workRequired the work that is required to complete this action
	 * @return returns true if the crew member was successful in completing their
	 *         action and false if they died
	 */
	private boolean everyAction(int workRequired) {
		// Increment the number of actions
		numberOfActions++;

		// Cap the work required between 0 and 30, add it to the tiredness
		int cappedWorkRequired = Math.min(Math.max(workRequired, 0), 30);
		tiredness += cappedWorkRequired;

		// Add an amount of the capped work required to the crew member's hunger
		// Depending on how tired they are
		if (tiredness >= 100) { // they are very tired
			tiredness = 100;
			hunger += 2 * cappedWorkRequired;
		} else if (tiredness >= 80) { // they are a little tired
			hunger += cappedWorkRequired;
		} else { // they are not tired
			hunger += cappedWorkRequired / 2;
		}

		if (hunger >= 100) { // if the crew member is very hungry
			// Subtract either 10, or how hungry they are; whichever is bigger
			health -= Math.max(10, hunger - 100);
			hunger = 100;
		} else if (hunger >= 80) { // if they are a little hungry just let them lose 5 health
			health -= 5;
		}

		// Check the crew member still has health?
		if (health <= 0) {
			die();
			return false;
		}
		return true;
	}

	private void die() {
		// Set their health to 0 and have no actions left
		health = 0;
		numberOfActions = 2;
	}

	/**
	 * Causes this crew member to consume the given item of food to decrease their
	 * hunger
	 * 
	 * @param food the item to be consumed by this crew member
	 * @return a string with a short message describing what happened
	 */
	public String eat(FoodItem food) {
		if (everyAction(5)) { // they use very little energy to eat something

			hunger -= food.nutritionalValue();
			hunger = Math.max(hunger, 0);
			parentCrew.removeItemFromInventory(food);

			return name + " consumed an item of " + food.toString() + " lowering his/her hunger to " + hunger;
		} else
			return name + "has died, OH NOOOO";
	}

	/**
	 * Cause this crew member to use the given medical item to increase their health
	 * 
	 * @param medic the medical item that will be used by this crew member
	 * @return a string with a short message describing what happened
	 */
	public String heal(MedicalItem medic) {
		int healAmount = Math.min(medic.healAmount(), getMaxHealth() - health);
		if (everyAction(5)) { // They use very little energy to do this

			health += healAmount;
			parentCrew.removeItemFromInventory(medic);

			if (medic.isSpacePlagueCure()) {
				hasSpacePlague = false;
				return name + " was cured of SpacePlague";
			}
			return name + " was healed after using a " + medic.toString() + " to raise his/her health to " + health;
		} else
			return name + "has died, OH NOOOO";
	}

	/**
	 * Allows this crew member to sleep and thus decrease their tiredness
	 * 
	 * @return a string with a short message describing what happened
	 */
	public String sleep() {
		if (everyAction(0)) {

			tiredness -= (type == CrewMemberTypes.Sleeper ? 100 : 60);
			tiredness = Math.max(tiredness, 0);

			return name + " had a refreshing sleep and lowered his/her tiredness down to " + tiredness;
		} else
			return name + "has died, OH NOOOO";
	}

	/**
	 * Makes this crew member repair the ship by 35 (or 50 for the mechanic)
	 * 
	 * @return a string with a short message describing what happened
	 */
	public String repairShip() {
		int repairAmount = (type == CrewMemberTypes.Mechanic ? 50 : 35);

		repairAmount = Math.min(repairAmount, 100 - parentCrew.parentGame.ship.getShieldHealth());
		if (everyAction(repairAmount)) {

			parentCrew.parentGame.ship.repair(repairAmount);

			return name + " repaired the ship's shields up to " + parentCrew.parentGame.ship.getShieldHealth();
		} else
			return name + "has died, OH NOOOO";

	}

	/**
	 * Makes this crew member explore the current plant in hope of finding an item
	 * or a transporter part.
	 * 
	 * @return a string with a short message describing what happened
	 */
	public String explorePlanet() {
		if (everyAction(type == CrewMemberTypes.Explorer ? 0 : 20)) {

			Item found = parentCrew.parentGame.planet.search();
			if (found == null) {
				return "Gutting " + name + " found nothing at all";
			} else {
				return name + " found a " + found.toString();
			}
		} else
			return name + "has died, OH NOOOO";
	}

	/**
	 * Makes this crew member use some energy to pilot the ship This method should
	 * only be called from the ship class
	 */
	public void pilotShip() {
		everyAction(type == CrewMemberTypes.Pilot ? 0 : 20);
	}

	/**
	 * Makes the crew member a little bit more tired as they move to the next day It
	 * also checks if this crew member has space plague and have positive health
	 * This method should only be called from the crew class
	 */
	public void nextDay() {
		tiredness += 5;
		tiredness = Math.min(tiredness, 100);
		numberOfActions = 0;

		if (hasSpacePlague)
			health -= 20;

		if (health <= 0)
			die();
	}

	/**
	 * Returns the image that represents this crew member type
	 * 
	 * @return an image of this crew member that is of a certain color
	 */
	public Icon getImageIcon() {
		final String dir = "/img/crew/";
		switch (type) {
		case Explorer:
			return new ImageIcon(this.getClass().getResource(dir + "blueExplorer.png"));
		case ExtraHealth:
			return new ImageIcon(this.getClass().getResource(dir + "greenExtraHealth.png"));
		case Mechanic:
			return new ImageIcon(this.getClass().getResource(dir + "orangeMechanic.png"));
		case Pilot:
			return new ImageIcon(this.getClass().getResource(dir + "redPilot.png"));
		case Sleeper:
			return new ImageIcon(this.getClass().getResource(dir + "yellowSleeper.png"));
		default:
			return new ImageIcon(this.getClass().getResource(dir + "blackDefault.png"));
		}
	}
}
