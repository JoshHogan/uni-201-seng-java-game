package main;

/**
 * Ship class stores information surrounding the ship and the random events that
 * can occur when it is used
 */

public class Ship {
	/**
	 * The game object that this ship is a part of
	 */
	private Game parentGame;
	/**
	 * The name of this ship that is chosen by the user
	 */
	private String name;
	/**
	 * A number representing the health of the shields of the ship, between 0-100
	 */
	private int shieldHealth;

	/**
	 * The maximum health of the shields of the ship
	 */
	private final int MAX_SHIELD_HEALTH = 100;

	/**
	 * Makes a new ship object which is user to transport the crew from planet to
	 * planet
	 * 
	 * @param parentGame the game that this ship is a part of
	 * @param name       the user chosen name of this ship
	 */
	public Ship(Game parentGame, String name) {
		this.parentGame = parentGame;
		this.name = name;
		shieldHealth = MAX_SHIELD_HEALTH;
	}

	/**
	 * Returns the shield health of this ship
	 * 
	 * @return a number between 0-100 representing the health
	 */
	public int getShieldHealth() {
		return shieldHealth;
	}

	/**
	 * Returns the name of the ship as chosen by the user
	 * 
	 * @return the name of the ship
	 */
	public String getName() {
		return name;
	}

	/**
	 * Repairs the shield of the ship by the given amount
	 * 
	 * @param amount the amount to repair the shield by
	 */
	public void repair(int amount) {
		shieldHealth += amount;
		shieldHealth = Math.min(shieldHealth, MAX_SHIELD_HEALTH);
	}

	/**
	 * Allows two crew members to pilot this ship to a new planet
	 * 
	 * @param selected1 the first pilot
	 * @param selected2 the second pilot
	 * @return a short message describing their trip
	 */
	public String pilot(CrewMember selected1, CrewMember selected2) {
		selected1.pilotShip();
		selected2.pilotShip();

		parentGame.planet = new Planet(parentGame);

		// String is empty if nothing happens
		String events = "";

		int chance = parentGame.rand.nextInt(6);
		if (chance == 0) {
			// 1 in 6 chance of hitting an asteroid belt
			events = "We HIT an asteroid belt, ship has taken damage and needs repairing\n"
					+ "The damage taken is affected by how strong the shields were prior to flying\n";

			int difference = MAX_SHIELD_HEALTH - shieldHealth;
			int damage = difference * 2 + 15;
			shieldHealth = Math.max(shieldHealth - damage, 0); // don't let it go below 0
		}
		return events + "The ship was piloted by: " + selected1.getName() + " and " + selected2.getName()
				+ " to a new planet called: " + parentGame.planet.getName();
	}

	/**
	 * A method that is called everyday to slightly lower the shield health of the
	 * ship by a certain amount. This should only be called from the game class
	 */
	public void nextDay() {
		shieldHealth -= 5;
		shieldHealth = Math.max(shieldHealth, 0);
	}

}
