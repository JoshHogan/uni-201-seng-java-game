package main;

import java.util.Random;

import main.CrewMember.CrewMemberTypes;

/**
 * Game class contains instances of each class, as well as the core mechanics of
 * the game itself (number of days, parts found, game completetion etc)
 */

public class Game {
	/**
	 * The crew object which contains a list of crew members and also the shared
	 * inventory of items
	 */
	public Crew crew;
	/**
	 * The ship that the crew use to fly from planet to planet
	 */
	public Ship ship;
	/**
	 * The space outpost where the crew can buy items
	 */
	public SpaceOutpost spaceOutpost;
	/*
	 * A planet where the crew members can explore to get items
	 */
	public Planet planet;
	/**
	 * The number of days that this mission will last, chosen by the user
	 */
	private int numberOfDays;
	/**
	 * The number of parts that are needed to be found for the mission to be
	 * successful, given by the numberOfDays*(2/3)
	 */
	private int numberOfParts;
	/**
	 * The current day that the mission is on, starts at 1
	 */
	private int currentDay;
	/**
	 * The number of parts that have been found by the crew
	 */
	private int partsFound;

	/**
	 * A random object that is used to generate random numbers for chance related
	 * things
	 */
	public Random rand;

	/**
	 * Makes a new game object which contains a crew, ship and also the outpost. The
	 * crew must complete a set mission by the user selected number of days
	 * 
	 * @param numberOfDays the number of days that this mission will last
	 * @param shipName     the name of the ship for this mission
	 */
	public Game(int numberOfDays, String shipName) {
		// Set-up the mission parameters
		this.numberOfDays = numberOfDays;
		numberOfParts = numberOfDays * 2 / 3;
		currentDay = 1;
		partsFound = 0;

		// Make the RNG
		rand = new Random();

		// Make new ship, crew, spaceOutpost and planet objects
		ship = new Ship(this, shipName);
		crew = new Crew(this);
		spaceOutpost = new SpaceOutpost(this);
		planet = new Planet(this);
	}

	/**
	 * Adds a new crew member to the crew, this is only used at the beginning of the
	 * mission
	 * 
	 * @param name the name of the crew member to add
	 * @param type a number representing the type of crew member
	 */
	public void addCrewMemeber(String name, CrewMemberTypes type) {
		crew.addMember(new CrewMember(crew, name, type));
	}

	/**
	 * Returns true if the game is finished, otherwise is false, the game will
	 * finish on a win or a lose
	 * 
	 * @return whether the game has finished or not
	 */
	public boolean gameComplete() {
		return partsFound >= numberOfParts || currentDay > numberOfDays;
	}

	/**
	 * @return the numberOfDays in the mission
	 */
	public int getNumberOfDays() {
		return numberOfDays;
	}

	/**
	 * @return the numberOfParts that the crew needs to collect
	 */
	public int getNumberOfParts() {
		return numberOfParts;
	}

	/**
	 * Returns the current day that the mission is on, starting from 1
	 * 
	 * @return the current day of the mission
	 */
	public int getCurrentDay() {
		return currentDay;
	}

	/**
	 * Returns the number of parts that have been found by the crew
	 * 
	 * @return the number of parts that have been found
	 */
	public int getPartsFound() {
		return partsFound;
	}

	/**
	 * Called when a crew member has found a part on a planet Increments the parts
	 * found by the crew by one.
	 */
	public void foundPart() {
		partsFound++;
	}

	/**
	 * Moves the game onto the next day by calling the next day methods for the crew
	 * and the ship, also increments the currentDay variable
	 * 
	 * @return a string describing what happened
	 */
	public String moveToNextDay() {
		currentDay++;
		ship.nextDay();
		crew.nextDay();
		int chance = rand.nextInt(10); // out of 10 chance

		if (chance == 0 && crew.getInventory().size() > 0) {
			// Alien pirates
			int chosenItemIndex = rand.nextInt(crew.getInventory().size());
			Item chosenItem = crew.getInventory().get(chosenItemIndex);
			crew.removeItemFromInventory(chosenItem);

			return "Oh NO!!\n Alien Pirates attacked and removed: " + chosenItem.toString()
					+ " from the crew's inventory";
		} else if (chance == 1 && crew.numberOfCrew() > 0) {
			// Space Plague
			int chosenCrewMemberIndex = rand.nextInt(crew.numberOfCrew());
			CrewMember chosenPerson = crew.getCrewMember(chosenCrewMemberIndex);
			chosenPerson.infect();

			return "OH NO!!\n" + chosenPerson.getName()
					+ " has fallen sick from space plague. Please heal them ASAP to avoid them losing health each day";
		} else {
			return "Moved to next day successfully, nothing eventful happened";
		}
	}
}
