package main;

import main.MedicalItem.MedicalItemType;
import main.FoodItem.FoodItemType;

/**
 * Planet class stores information surrounding the different planets they player
 * can fly to, as well as the chance of random events occurring when exploring;
 * including finding different items from other classes
 */

public class Planet {
	/**
	 * A long list of the possible names of the planets
	 */
	private static String[] planetNames = { "Lognothea", "Zonguria", "Yibrao", "Sethone", "Saenus", "Lunov", "Zopiria",
			"Strahuclite", "Strone F0SA", "Gnippe N", "Llora NW9F", "Bora 824", "Llides 900R", "Llillon AEL",
			"Lliea 05", "Gerth WG5Q", "Brides 280", "Zippe D0DZ", "Zorth 2C0V", "Droth YO8J", "Strov UY3U", "Nion 66P",
			"Milles 4", "Cosie N2K", "Zypso 5UQF", "Drerth AF", "Thoth G7E", "Voth 51P", "Zuna DFCR", "Photh K3RY",
			"Lao R8", "Geon 4E", "Mosie 5PV", "Thurn 2SV", "Lion 5T2", "Phov BG70", "Crichi L1XC", "Nippe 91U",
			"Llapus D71", "Diea TIO", "Trion RFW3", "Vosie 4FHJ", "Strov J173", "Milles JG0", "Gnov T6I", "Drarth 14O",
			"Llarth XIK", "Gnillon 4H", "Brion 9FZ", "Nora MD1", "Sonoe ON79", "Varvis QUF", "Choria 9M3", "Veron UYN",
			"Greshan 01J", "Droria V580", "Thorix 44H", "Llorth 47K1", "Drora CON", "Brichi 56HI", "None 0E6",
			"Brurn M0L", "Chao JU", "Nides U5", "Zoria 8R9", "Vagua 13", "Morth 3O2", "Strypso 8", "Crars 6Y8",
			"Llypso 7X5", "Grolla 66Z", "Strypso T56", "Bromia 2W3W", "Gninda V6A", "Vomia 5OD", "Droria Q",
			"Streshan 9I", "Cheon 7YYF", "Choria W3F2", "Thosie 68E", "Llion 374", "Gagua 4U16", "Sao BB7Y",
			"Chilles 7427", "Veshan 2CB", "Zomia JSB0", "Trao WX", "Muna XX", "Luna BRI", "Llerth 02U", "Mion N8S",
			"Gars 62", "Trorth 4Q", "Nore 655", "Thonoe M6XG", "Streshan I9G", "Zarth W7CS", "Lorth 66Z", "Philles 3HY",
			"Criri 4VR", "Zyke 1712", "Lypso NJQ", "Zore 1F1", "Donoe A02", "Lippe 2BV0", "Chorix Q", "Cone 9QR",
			"Bides Q357", "Thosie RBS", "Chagua 768S", "Chone MA", "Niri J6", "Trov 40", "Vuna O2P", "Ceon 35M",
			"Drone TLA5", "Phov KN8", "Nuna 8TJG", "Nerth TOA", "Cides QYG", "Gnides 1PE", "Llorix 9H", "Grillon XTE3",
			"Criea GII", "Nion XB", "Phone XP", "Criuq 1GJ", "Badus 6", "Dryke 4", "Grarvis F2", "Vinda A4Y",
			"Niea SN4G", "Dryria I58", "Dinda 91A", "Brov 21", "Vadus 2ED2", "Lleon BC7", "Gnyke 4ELG", "Theron 29D",
			"Gara SB", "Phomia XB31", "Llyke UIL", "Donoe R7K", "Brolla IC", "Llosie D6X", "Churn 730", "Gara ZIZ",
			"Merth O9M7", "Thonoe Y9X8", "Crosie B8V", "Gragua 3EQ", "Ginda 3NE", "Phurn KFJ8", "Gapus MKL5",
			"Strinda V9", "Billes TZ7", "Sinda F3", "Muna 5UV", "Cuna 874M", "Gnerth T5K7", "Vilia 8KE", "Brarth PGIG",
			"Zilia XE", "Drorth ", "Treron UU4Y", "Sore O7A", "Dyke 2MM", "Greshan DGM", "Cryke 54M8", "Bolla 40S",
			"Llion 9V", "Vion RRE", "Gnion 8TZ6", "Larth T09M", "Tragua 1X", "Grorth W750", "Binda 0G7", "Strorix P3",
			"Lion F5", "Phara J1OL", "Dreron 614", "Chone 9U", "Trides D", "Gnyria EQGW", "Groria ", "Llapus Y7",
			"Dyke HRR", "Llorix 08U", "Zorth TS1D", "Vides HVT", "Liri KO4", "Diri A7Q", "Pheron AN1", "Siea TQY",
			"Thomia 6I7Y", "Philles 6664", "Dore 6352", "Surn 5HZ9", "Bomia Y844", "Lides Q9V", "Mapus Q5V", "Vichi OZ",
			"Strilles 95", "Lapus MSL", "Berth 65L", "Phora Z7", "Moria 3AD", "Bryria A1", "Vapus MZ2X", "Drorix XLKB",
			"Bagua 4N1", "Chapus 5P", "Griea WT00", "Zao MAV", "Gneshan 73DZ", "Lyke 20NJ", "Thara 9V6A", "Strone WLX",
			"Silia XY9", "Thade L84", "Barth 33AY", "Narvis GST6", "Gagua 14K2", "Donoe E0D2", "Griri T8P8", "Durn KLA",
			"Troth Q03", "Gars 84N", "Lides 7B4", "Grorix 4I01", "Bars 6M5M", "Gnerth 3377", "Grolla 72X5",
			"Drora KN96", "Nara UW", "Chorix GA5", "Serth 9SB", "Gnarvis RQK7", "Monoe 4K4", "Grion 639", "Thara DJ5",
			"Grurn D1", "Phides 722", "Thiuq 1D9", "Billon S9G", "Strarth 1084", "Zade H800", "Cara C7O", "Gonoe Z408",
			"Chyria O11", "Drade Y8", "Vadus 8OKA", "Colla 9123", "Neron F8", "Comia Y4", "Ceon 3IC", "Cinda FE12",
			"Beshan 57", "Gyria T7", "Bryria SC", "Biea 36J", "Chuna C4S5", "Dosie X4MU", "Chorth 19L", "Nillon N3O",
			"Lora P1", "Gnonoe M73", "Morth U", "Volla 22PO", "Noth 33G0", "Stragua 8620", "Momia 0VY", "Comia ZV0X",
			"Brurn K9T", "Nade TK5", "Crars 2E", "Strapus EW", "Cilia Q73", "Bars O86", "Zypso NWUP", "Strypso G88",
			"Cara 85", "Dars K5FJ", "Pheron A08", "Curn UG", "Cides C4", "Greshan ILX", "Geshan D2J9", "Therth 4DD",
			"Nion 8W", "Sonoe 3926", "Cryria 515C", "Briri KY2N", "Veron A4", "Chuna VUBU", "Drara 951F", "Driea CU0",
			"Comia RZG", "Lapus 091", "Gnurn X4", "Gagua L94I", "Phao P1B", "Gragua I6ZA", "Vilia DRG", "Lapus CXO0",
			"Noria G17", "Mippe XLI7", "Grion 2EP", "Gnadus 5N3P", "Llora 7IF", "Sillon JMW", "Nillon 4VA1",
			"Trides GVZ2", "Nillon 1", "Gnadus 27", "Grorix ZCYK", "Phore 567T", "Gnora 404", "Grinda LN7", "Siri 07",
			"Gromia 34AN", "Phara EE2", "Grolla 70J", "Groria 2UR6", "Llagua 4TB", "Goria F3P", "Treshan R7FH",
			"Grorth 8BR6", "Both AB", "Sarth K5I", "Zyke PST3", "Miea 20", "Veon JYO", "Vyke SNFD", "Phides 859",
			"Gars O2I", "Cholla OV9Q", "Gnerth CH3", "Bichi LN", "Vypso 4", "Diuq 2J5", "Vichi 3LY8", "Trara 88",
			"Dronoe EAW", "Grora DYKV", "Dreron 3RL", "Thilles LCZ4", "Gneshan K80V", "Gypso S8I", "Creon S2VR",
			"Dagua M7W", "Brurn Y4", "Llorth X7X", "Pheron 79", "Both VEQ", "Dryria 18F", "Vars ZSS", "Chichi 6Z",
			"Cheron SF3V", "Trosie 891", "Corix D5LN", "Zoria QYO8", "Zore 48", "Chorix 5ZTT", "Darvis AK84",
			"Gnore ML", "Bilia W3BY", "Croria X71", "Breshan Q2X", "Nuna ", "Bippe D", "Monoe 1WMS", "Lyke J4B",
			"Dyria 9V", "Trilia 3OPL", "Thion JH3W", "Crion O5", "Gora 4RUG", "Lion IEK", "Llone G3", "Cilia YDXH",
			"Thichi FJ0B", "Gneshan X6K", "Gnars S67", "Madus 8WN", "Chion RIQW", "Druna H9FC", "Zoria O0Q",
			"Liri JQ4R", "Grinda 4U9C", "Croria 57T5", "Cosie 068E", "Drara C", "Zonoe PYEE", "Broria Q4J", "Gides B8Z",
			"Sichi I9Q", "Nara 67I", "Groth NU", "Vadus H", "Sadus N4T", "Brarth PLM", "Thilles GI78", "Cov WW62",
			"Drinda BY", "Thonoe IF", "Bonoe TOT", "Phiuq DR4", "Cronoe M8H7", "Gronoe S", "Drorth I98", "Crilia J0O",
			"Stradus 5H7", "Dara VD54", "Murn 021", "Viuq SS2", "Sars K5B5", "Therth 9", "Gnyke NDG4", "Gnara AZL",
			"Phone RSH", "Brao 49AC", "Gorth R8RA", "Lleshan 4DC", "Gnides 10UV", "Drosie 9", "Narth 3A82", "Llorix 7R",
			"Ziri W5TX", "Liri O4M", "Treron 3PW", "Voth 4VZ3", "Grolla R3", "Llomia 0DX4", "Dyria 76E4", "Mov IKN",
			"Thippe NTRV", "Llomia K0HM", "Zao 34P", "Phore VLL0", "Thoria 6A5", "Stragua 474", "Siri RVC", "Gao 7E",
			"Llosie K", "Crion 3Z", "Gnorth 838", "Chion A0", "Soth W4N", "Brilles MN", "Byria 4042", "Soria IYH4",
			"Stradus L5", "Phion D6", "Bone DB", "Mora VY3", "Cion MK", "Llion B1L7", "Gnone UO", "Gao MJXN",
			"Dion 9D6I", "Grilles 354X", "Brichi J", "Pharvis VCZR", "Driri PXCA", "Crides 14WQ", "Goth X",
			"Phyria 6WV", "Stragua KP3U", "Llone X", "Zichi NO7", "Phoria P2X", "Bov 26", "Phides 771", "Bara 3V",
			"Pheron HK9", "Tholla 43G", "Gnypso 8W8N", "Zolla IT9", "Thora DM7L", "Grinda 80ES", "Gerth XAGW",
			"Ginda 39YZ", "Guna EBWN", "Cragua V79", "Llippe 9F3", "Chars XDC", "Deshan 81K", "Ziri 7MU", "Strara G1IT",
			"Llars EGX", "Sore 9S", "Zars 2TGR", "Creon G2J", "Phichi 8L87", "Breshan ENJ3", "Brion O1", "Veron 198O",
			"Drars MP3", "Dides 9EQ", "Giea Z", "Gninda LU4", "Zuna NH", "Luna 7", "Nypso T66", "Zeon 4R9",
			"Narvis 43N", "Bragua ZMS", "Strillon PC8", "Brarvis BN9", "Zara 5KT", "Strore PQQJ", "Darvis 07I",
			"Bomia HW2", "Biri 596Y", "Zerth 6QY", "Nade T5", "Cade 9519", "Llides 36", "Phillon WE42", "Phars QDP",
			"Crorix 9WF", "Nolla 440V", "Cinda 7A2", "None Z3B", "Chichi 3XO", "Dides 11Z1", "Niuq PZI", "Drov 4AP8",
			"Drides 0FDC", "Brorix 76", "Grarth 0YA", "Merth I27T", "Nion 9GK6", "Magua K8N", "Gryria WUJ7", "Lyke O2E",
			"Breshan 8FP", "Norix V0K", "Cilles F02K", "Meon U9Q", "Vyke X6", "Streshan 44", "Triea 3MA", "Cichi EUI",
			"Creshan 451", "Grora 6L73", "Stromia I", "Tragua 86", "Vore SG6D", "Zypso 4INT", "Zoria 4", "Cinda 15K",
			"Millon EN6K", "Lone 95R", "Pharvis 06CL", "Surn JYR", "Gorix V52Y", "Creon 9NY", "Nao 7V", "Sars W2",
			"Strosie 3Z", "Brolla B7WW", "Dorth 4", "Lluna M10", "Nolla 6O", "Bolla BOJ4", "Phade R1U", "Voth N237",
			"Liuq ROOV", "Trurn 3OG", "Mone CJSO", "Gichi TB", "Llerth 8NDP", "Bagua 7L1P", "Ciuq A42N", "Triea 1Y",
			"Gronoe F52", "Grarvis I2", "Niri D58C", "Goth S3MC", "Thars 19I", "Drypso EGY", "Meon VYFV", "Lliuq EGH5",
			"Zagua 1QOZ", "Thion 9GG", "Stromia OJT", "Strarvis XZ74", "Vosie 52", "Drara 09P", "Pharvis RHG",
			"Thosie AN9", "Viea GVD", "Zeshan N59", "Llyke 9M5X", "Zara 2P4", "Neron C4ZX", "Gnyria 3Y", "Gnonoe 3821",
			"Lliuq HKD", "Gnars Z70", "Varth 9FP", "Liea X6YX", "Cronoe 6968", "Chippe G8M", "Zonoe BG9", "Vars HI",
			"Guna U65", "Gnore 0I", "Cade X7", "Trao 7RT0", "Bromia WP05", "Mippe 1699", "Drarvis 5G", "Crorix P44",
			"Striuq 21", "Vone TO", "Tragua W05L", "Vurn 2B0", "Phiri N4W8", "Nade 112", "Gnorth K8", "Mion SL",
			"Strorix GG", "Zeron XE26", "Lilles 031S", "Sarvis C4", "Gov CI9", "Mao QH6", "Myke Y6Q", "Grinda 61ZI",
			"Marvis 38J8", "Dolla 38B", "Pheon ZWKS", "Dronoe 5V7", "Drara 6", "Nagua 6YCW", "Thippe 141N", "Nora F02",
			"Nars A4P", "Charvis O0PF", "Gnadus LJ9S", "Crone ", "Trora DTD", "Nolla 3OKM", "Strov 64XJ", "Grichi H4J",
			"Phone 7Q2P", "Bippe 1H3", "Treron 7FC", "Strion U50S", "Dov YD5", "Pharvis ", "Llars JGNU", "Moth O9",
			"Greron 5BR0", "Crade 28E1", "Bromia TV6G", "Derth 9DOW", "Veon EA", "Zolla 4V", "Suna UVI", "Nion 1I",
			"Brade L3DQ", "Cilles Y2", "Llides VD", "Grorix JS24", "Gnorix XO", "Cragua 8DI5", "Vov MZQ5", "Trurn Q9F",
			"Troth 03", "Trippe 1027", "Mides 608", "Nara ZIQ0", "Zorth 3C3", "Trillon XDB", "Criea TP80", "Berth R",
			"Nerth Y39", "Nyke ZXD", "Napus A83", "Striea 025B", "Milles 1NV", "Niri IFQ", "Zore SI0", "Zorth SN66",
			"Tharth 6ZW", "Gryria L57D", "Strars KZ7", "Milia 75X", "Sore 8XT9", "Vippe 7553", "Trars S0B",
			"Brilles Y89", "Tronoe 9LL8", "Chonoe KTEV", "Nion BA", "Dides D98U", "Stryke A9N", "Voria 992",
			"Trinda MS", "Monoe 6SLA", "Loth SIGR", "Zyria UK0", "Llichi 1", "Griea UFM4", "Brichi GU", "Darth OKW1",
			"Strippe S0W9", "Llagua EBTX", "Varvis X9K7", "Crapus 424", "Viea Z8G", "Lleron 5RL3", "Phurn DU3R",
			"Llonoe A", "Lade 76", "Zorix 82RP", "Brillon DL", "Chinda 3S4", "Striri V", "Cora 7UAI", "Nosie FZ0",
			"Larth 9SRV", "Bruna 4G2", "Zeshan N1", "Borth GKM", "Donoe WQ", "Silia DV", "Meon 119", "Llao 99",
			"Grapus WJX1", "Brov 7FV3", "Choth 596", "Chiea QRP", "Burn 8Y2", "Bade R5S", "Bides 5MO", "Thion U8G",
			"Narvis 7R3F", "Zara DU7P", "Ciuq A1H", "Trippe I53T", "Gnarvis 02D", "Vorth M", "Criri 624", "Lerth 9Z",
			"Dov RA", "Vinda 2A68", "Sion 6U", "Lion MUL", "Llion C21", "Gurn P4K", "Drurn OC9", "Larth MMJ",
			"Trorix 04", "Gion KM5M", "Cherth 5B30", "Lerth JS1", "Brypso 90", "Driea 57", "Crion 4INC", "Streshan R",
			"Vadus 9V3", "Cichi M0D", "Lilles S37", "Somia Y05", "Guna 9NB0", "Phiri 5RM", "Byke GBN", "Crora A7T4",
			"Gnarth DH5T", "Gara 01", "Barvis 7", "Gars PT", "Vorix 6M", "Nara 397Y", "Gromia IO", "Cradus 22M0",
			"Cilles F9L", "Bomia LHA7", "Nore CO4", "Sagua B3O", "Garth GT", "Bichi A6PM", "Strillon A0IW",
			"Drillon 9MO3", "Leon 014", "Zomia Q9", "Borth HSD8", "Trone 65LB", "Syke C9T", "Dore ADZ6", "Gnyke NGT6",
			"Trurn XJ5", "Broria D74", "Niri 5L", "Vao FWWC", "Strosie ZC", "Miuq 9CV", "Breon S1UC", "Gnillon MP2",
			"Zerth 7", "Gyria 1IQ9", "Giuq F37T", "Trypso 12U", "Ciri NO", "Gadus I1W", "Gninda JHD", "Strosie 5XP4",
			"Sonoe ST1E", "Garth CF2", "Llars 7WW5", "Cars L94", "Gromia WK", "Cion TWEN", "Gnilia 1DH", "Siri 85",
			"Gronoe VBH1", "Thadus G2", "Linda 8FS", "Mides J6J", "Croth F0WM", "Gnorix 4VLA", "Llapus ZK",
			"Bragua UY48", "Crypso BGI", "Verth EEM", "Nars 79", "Zinda 6T2", "Chinda 9076", "Llolla 7423", "Deron N6J",
			"Chilles T", "Milia 17ML", "Zarth 3G3", "Liri T8", "Gneron 4ZF2", "Vadus 1282", "Bagua 9TUW", "Cyke 2",
			"Llion QDH", "Phuna 6JV", "Borix 5", "Vars IK6", "Gilles 9VQ7", "Syke 3IBW", "Stryria UE8", "Lagua EX",
			"Strypso 21U", "Grone Y", "Loria OF", "Truna 3ZP", "Grao WG0T", "Triuq K35", "Zinda OPJ", "Zyke 7",
			"Gnorix WY7", "Billon YM7J", "Thoria 81R8", "Gneshan 07", "Llolla 6JX", "Nypso 25MV", "Struna V47",
			"Phion O5Q", "Vion 59", "Vadus 74MX", "Vuna FQZP", "Cherth 67T7", "Strilia 3S9", "Sagua NCLG", "Lluna JF",
			"Tharvis GQ", "Truna 2V", "Sorix MVC7", "Muna 6G", "Biri 2J3", "Brore 5R4O", "Gnerth F548", "Drichi T3V0",
			"Chilles 0EG", "Lliea WG0", "Gnapus E1", "Trars 235", "Gilia TJS", "Dora 8S1Q", "Chypso 3258", "Dorth 99",
			"Treon Q3", "Llorth NB", "Drarth GZ4", "Gryria IL05", "Darvis 65C", "Drarvis F9O7", "Llarvis AN7",
			"Nadus W2", "Giuq K81", "Greon CZ5", "Muna 5Q0", "Vypso C7", "Triri NN96", "Meron 4", "Bagua NUO",
			"Dars V2P", "Dade 9I1H", "Sion K2", "Bade 88S", "Gypso QK7", "Drosie 0JY6", "Miri 012", "Varth P76",
			"Stroria TM2", "Narvis 4G6", "Gnerth 4B", "Drov 096O", "Vapus V70U", "Trolla BTVN", "Gnorth 0W0K",
			"Cheon 583", "Greon 1C", "Broria 4503", "Cion 9EG8", "Niea 7F4", "Lilia Y9O", "Chagua 88T6", "Criuq RX4",
			"Creron DS", "Strapus N20", "Millon LU0", "Linda XFL", "Llyria 5IM", "Crilles 9JY", "Mion 0GC", "Grion 92U",
			"Crillon 53Q", "Dreron NC", "Lypso 18P", "Ceron E89W", "Gov 6DK8", "Drarvis ONU6", "Brao F8Y", "Gnion Y",
			"Lyke M9RM", "Molla C7", "Phov ILA", "Brides QF5T", "Vara E21", "Driri G2S", "Mapus GF", "Nara 8",
			"Griuq J1FM", "Trion 781", "Pheron 7482", "Ceron 24H", "Trides 4H5", "Phora FK1Z", "Gnion MO14", "Beshan I",
			"Thypso K6Y1", "Biri 7BJF", "Theon 3UI", "Doria O4", "Cichi 92L9", "Choth AHV", "Liri 8X", "Vides H5W",
			"Suna 0R9V", "Veon 0J1", "Phomia LHQ", "Cherth Z61", "Trion 2D9U", "Sion 7K6", "Lloria 26U", "Gnov 72",
			"Phov 7", "Trion 5S9N", "Zyke FWIK", "Lloth RF", "Nilles 6RLF", "Cora QA9T", "Trilia 1LJS", "Michi E06",
			"Ceshan 90D6", "Zao 5D2", "Lao 50", "Ladus M25", "Verth J4", "Cryke VD", "Thore SH", "Brao YQ", "Myke I0",
			"Chapus KGB", "Gides 088", "Cuna O3Z", "Brilles 03UN", "Strurn A3", "Meron 0", "Tryria F35X" };
	/**
	 * The parent game that this planet is a part of
	 */
	private Game parentGame;
	/**
	 * Whether a transporter part has been found on this planet or not
	 */
	private Boolean foundTransporterPart;

	/**
	 * The name of this planet
	 */
	private String name;

	/**
	 * Makes a new planet which is a part of the given game The name of this planet
	 * is randomly chosen from a list
	 * 
	 * @param parentGame the game that this planet is a part of
	 */
	public Planet(Game parentGame) {
		this.parentGame = parentGame;
		foundTransporterPart = false;
		// Choose a random name from the list
		name = planetNames[parentGame.rand.nextInt(planetNames.length)];
	}

	/**
	 * Search the planet in hope of finding an item. This is called from the crew
	 * member class when a certain crew member is sent to explore the planet
	 * 
	 * @return The item that has been found or null if nothing has been found
	 */
	public Item search() {
		int randomChoice;
		Item found = null;
		int index = 0;

		// It is only possible to find one transporter part per planet
		if (foundTransporterPart)
			randomChoice = parentGame.rand.nextInt(4);
		else
			randomChoice = parentGame.rand.nextInt(5);

		switch (randomChoice) {
		case 0: // food item
			index = parentGame.rand.nextInt(FoodItemType.values().length);
			found = new FoodItem(FoodItemType.values()[index]);
			parentGame.crew.addItemToInventory(found);
			break;
		case 1: // medical item
			index = parentGame.rand.nextInt(MedicalItemType.values().length);
			found = new MedicalItem(MedicalItemType.values()[index]);
			parentGame.crew.addItemToInventory(found);
			break;
		case 2: // money bag
			index = parentGame.rand.nextInt(MoneyBag.amounts.length);
			found = new MoneyBag(MoneyBag.amounts[index]);
			parentGame.crew.bankAccount += found.price();
			break;
		case 3: // found nothing
			break;
		case 4: // transporter part
			foundTransporterPart = true;
			found = new TransporterPart();
			parentGame.foundPart();
			break;
		}
		return found;
	}

	/**
	 * @return the name of this planet
	 */
	public String getName() {
		return name;
	}
}
