package main;

/**
 * TransporterPart class contains the information about the transporter part,
 * inherits from item but cannot be bought in game (only found while exploring)
 */

public class TransporterPart extends Item {

	@Override
	public ItemType getItemType() {
		return ItemType.TransporterPart;
	}

	@Override
	public int price() {
		return 1000;
	}

	@Override
	public String toString() {
		return "Transporter Part";
	}

	@Override
	public Item clone() {
		return new TransporterPart();
	}
}
