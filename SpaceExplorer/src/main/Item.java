package main;

/**
 * Item is an abstract class information surrounding each item that can be
 * bought from the space shop
 */

public abstract class Item implements Comparable<Item> {
	public enum ItemType {
		TransporterPart, Medical, Food, MoneyBag
	}

	/**
	 * Returns the type of this item, examples are Transporter Part, Medical or
	 * Food.
	 * 
	 * @return the type of this item
	 */
	public abstract ItemType getItemType();

	/**
	 * Returns the price of this item to buy at the outpost
	 * 
	 * @return the price of this item
	 */
	public abstract int price();

	@Override
	public abstract String toString();

	@Override
	public abstract Item clone();

	// This already has documentation
	public int compareTo(Item otherItem) {
		if (getItemType() == otherItem.getItemType()) {
			return price() - otherItem.price();
		} else {
			return this.getItemType().compareTo(otherItem.getItemType());
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o.getClass() != getClass())
			return false;
		Item item = (Item) o;
		return compareTo(item) == 0;
	}
}
