package tests;

import org.junit.Before;
import org.junit.Test;

import main.*;
import main.CrewMember.CrewMemberTypes;
import main.FoodItem.FoodItemType;
import main.MedicalItem.MedicalItemType;

public class TestCrewMember {

	Game game;
	CrewMember crewMember;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "Shipo");
		game.addCrewMemeber("Josh", CrewMemberTypes.Plain);
		crewMember = game.crew.getCrewMember(0);
	}

	@Test
	public void testCrewMember() {
		assert (crewMember.health == 100);
		assert (crewMember.hunger == 0);
		assert (crewMember.tiredness == 0);
		assert (crewMember.getNumberOfFreeActions() == 2);
	}

	@Test
	public void testGetName() {
		assert (crewMember.getName().equals("Josh"));
	}

	@Test
	public void testGetType() {
		assert (crewMember.getType().contentEquals("Plain"));
		assert (crewMember.getImageIcon().getIconHeight() > 0);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Mechanic);
		assert (crewMember.getType().equals("Mechanic"));
		assert (crewMember.getImageIcon().getIconHeight() > 0);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Explorer);
		assert (crewMember.getType().equals("Explorer"));
		assert (crewMember.getImageIcon().getIconHeight() > 0);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Sleeper);
		assert (crewMember.getType().equals("Sleeper"));
		assert (crewMember.getImageIcon().getIconHeight() > 0);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.ExtraHealth);
		assert (crewMember.getType().equals("Extra-Health"));
		assert (crewMember.getImageIcon().getIconHeight() > 0);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Pilot);
		assert (crewMember.getType().equals("Pilot"));
		assert (crewMember.getImageIcon().getIconHeight() > 0);
	}

	@Test
	public void testGetNumberOfFreeActions() {
		assert (crewMember.getNumberOfFreeActions() == 2);
		assert (crewMember.freeAction());

		crewMember.sleep();
		assert (crewMember.getNumberOfFreeActions() == 1);
		assert (crewMember.freeAction());

		crewMember.pilotShip();
		assert (crewMember.getNumberOfFreeActions() == 0);
		assert (crewMember.freeAction() == false);
	}

	@Test
	public void testInfect() {
		assert (crewMember.hasSpacePlague == false);
		crewMember.infect();
		assert (crewMember.hasSpacePlague);
	}

	@Test
	public void testEat() {
		crewMember.hunger = 50;
		crewMember.eat(new FoodItem(FoodItemType.ButterChicken));
		assert (crewMember.hunger < 50);

		// Kill him by eating
		crewMember.health = 0;
		assert (crewMember.eat(new FoodItem(FoodItemType.Bacon)).contains("died"));
	}

	@Test
	public void testHeal() {
		crewMember.infect();
		// Damager them a little from space plague
		crewMember.nextDay();
		assert (crewMember.health < 100);

		crewMember.heal(new MedicalItem(MedicalItemType.SpacePlagueCure));
		assert (crewMember.hasSpacePlague == false);

		crewMember.heal(new MedicalItem(MedicalItemType.FullHeal));
		assert (crewMember.health == 100);

		crewMember.health = 0;
		assert (crewMember.heal(new MedicalItem(MedicalItemType.SpacePlagueCure)).contains("died"));
	}

	@Test
	public void testSleep() {
		crewMember.tiredness = 100;
		crewMember.sleep();
		assert (crewMember.tiredness == 40);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Sleeper);
		crewMember.tiredness = 100;
		crewMember.sleep();
		assert (crewMember.tiredness == 0);

		crewMember.health = 0;
		assert (crewMember.sleep().contains("died"));
	}

	@Test
	public void testRepairShip() {
		for (int i = 0; i < 30; i++) {
			game.ship.nextDay();
		}
		crewMember.repairShip();
		assert (game.ship.getShieldHealth() == 35);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Mechanic);
		crewMember.repairShip();
		assert (game.ship.getShieldHealth() == 35 + 50);

		crewMember.health = 0;
		assert (crewMember.repairShip().contains("died"));
	}

	@Test
	public void testExplorePlanet() {
		int found = 0;
		for (int i = 0; i < 20; i++) {
			if (crewMember.explorePlanet().contains("found"))
				found++;
		}
		assert (found > 1 && found < 18);
		assert (crewMember.health == 0);
		assert (crewMember.explorePlanet().contains("died"));

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Explorer);
		crewMember.explorePlanet();
		assert (crewMember.tiredness == 0);
	}

	@Test
	public void testPilotShip() {
		crewMember.pilotShip();
		assert (crewMember.tiredness == 20);

		crewMember = new CrewMember(game.crew, "Josh2", CrewMemberTypes.Pilot);
		crewMember.pilotShip();
		assert (crewMember.tiredness == 0);
	}

	@Test
	public void testNextDay() {
		assert (crewMember.tiredness == 0);
		crewMember.nextDay();
		assert (crewMember.tiredness == 5);

		crewMember.infect();
		for (int i = 0; i < 5; i++) {
			crewMember.nextDay();
		}
		assert (crewMember.health == 0);
		assert (crewMember.getNumberOfFreeActions() == 0);
	}
}
