package tests;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import main.*;

public class TestPlanet {

	Game game;
	Planet planet;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "Ship");
		planet = game.planet;
	}

	@Test
	public void testPlanet() {
		ArrayList<String> names = new ArrayList<String>();

		// Check for name uniqueness
		for (int i = 0; i < 10; i++) {
			assert names.contains(planet.getName()) == false;
			names.add(planet.getName());
			planet = new Planet(game);
		}
	}

	@Test
	public void testSearch() {
		// check we can only find one transporter part per planet
		for (int i = 0; i < 50; i++) {
			planet.search();
		}
		assert game.getPartsFound() == 1;
	}
}
