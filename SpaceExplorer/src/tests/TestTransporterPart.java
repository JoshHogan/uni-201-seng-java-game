package tests;

import org.junit.Before;
import org.junit.Test;

import main.Item;
import main.Item.ItemType;
import main.TransporterPart;

public class TestTransporterPart {

	Item transporterPart;

	@Before
	public void setUp() throws Exception {
		transporterPart = new TransporterPart();
	}

	@Test
	public void testGetItemType() {
		assert transporterPart.getItemType() == ItemType.TransporterPart;
	}

	@Test
	public void testPrice() {
		assert transporterPart.price() == 1000;
	}

	@Test
	public void testToString() {
		assert transporterPart.toString().equals("Transporter Part");
	}

	@Test
	public void testClone() {
		assert transporterPart.clone() instanceof TransporterPart;
	}

}
