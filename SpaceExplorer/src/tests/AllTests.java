package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestCrew.class, TestCrewMember.class, TestGame.class, TestPlanet.class, TestShip.class,
		TestSpaceOutpost.class, TestTransporterPart.class, TestMoneyBag.class, TestMedicalItem.class,
		TestFoodItem.class, TestItem.class })
public class AllTests {
}
