/**
 * 
 */
package tests;

import org.junit.Before;
import org.junit.Test;
import main.*;
import main.CrewMember.CrewMemberTypes;
import main.MedicalItem.MedicalItemType;

/**
 * @author Josh
 *
 */
public class TestGame {

	Game game;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "Cool Ship");
	}

	@Test
	public final void testGame() {
		for (int i = 0; i < 20; i++) {
			game = new Game(i, "Cool Ship " + i);
			assert (game.getPartsFound() == 0);
			assert (game.getCurrentDay() == 1);
			assert (game.getNumberOfDays() == i);
			assert (game.getNumberOfParts() == i * 2 / 3);
			assert (game.ship.getName().equals("Cool Ship " + i));
		}
	}

	@Test
	public final void testAddCrewMemeber() {
		assert (game.crew.getCrewMembers().size() == 0);
		game.addCrewMemeber("Josh", CrewMemberTypes.Mechanic);
		assert (game.crew.getCrewMembers().size() == 1);
	}

	@Test
	public final void testGameComplete() {
		assert (game.gameComplete() == false);
		for (int i = 0; i < 4; i++) {
			game.moveToNextDay();
		}
		assert (game.gameComplete() == true);

		game = new Game(3, "Ship");
		// we need two parts
		game.foundPart();
		assert (game.gameComplete() == false);
		game.foundPart();
		assert (game.gameComplete() == true);
	}

	@Test
	public final void testGetCurrentDay() {
		for (int i = 1; i <= 4; i++) {
			assert (game.getCurrentDay() == i);
			game.moveToNextDay();
		}
	}

	@Test
	public final void testGetPartsFound() {
		assert (game.getPartsFound() == 0);
		for (int i = 1; i <= 2; i++) {
			game.foundPart();
			assert (game.getPartsFound() == i);
		}
		assert (game.gameComplete() == true);
	}

	@Test
	public final void testmoveToNextDay() {
		boolean alienPirates = false;
		boolean spacePlague = false;

		for (int i = 0; i <= 100; i++) {
			game = new Game(5, "Shipo");
			game.addCrewMemeber("Josh", CrewMemberTypes.Mechanic);
			game.crew.addItemToInventory(new MedicalItem(MedicalItemType.FullHeal));

			// move forward two days
			game.moveToNextDay();
			game.moveToNextDay();

			if (game.crew.getInventory().size() == 0)
				alienPirates = true;
			if (game.crew.getCrewMember(0).health < 100)
				spacePlague = true;
		}
		assert (spacePlague);
		assert (alienPirates);
	}
}
