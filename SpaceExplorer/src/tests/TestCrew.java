package tests;

import org.junit.Before;
import org.junit.Test;

import main.*;
import main.CrewMember.CrewMemberTypes;
import main.FoodItem.FoodItemType;
import main.MedicalItem.MedicalItemType;

public class TestCrew {

	Game game;
	Crew crew;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "Cool Ship");
		crew = game.crew;
	}

	@Test
	public void testCrew() {
		assert (crew.parentGame == game);
		assert (crew.freeCrewMembers() == 0);
		assert (crew.numberOfCrew() == 0);
		assert (crew.bankAccount == 500);
	}

	@Test
	public void testAddMember() {
		for (int i = 1; i <= 4; i++) {
			game.addCrewMemeber("Josh " + i, CrewMemberTypes.Mechanic);
			assert (crew.freeCrewMembers() == i);
			assert (crew.numberOfCrew() == i);
			assert (crew.getCrewMember(i - 1).getName().equals("Josh " + i));
			assert (crew.getCrewMembers().size() == i);
		}
	}

	@Test
	public void testAddRemoveItemInventory() {
		assert (crew.getInventory().size() == 0);

		// Add three items to inventory
		Item item1 = new MedicalItem(MedicalItemType.FullHeal);
		Item item2 = new FoodItem(FoodItemType.Bacon);
		Item item3 = new FoodItem(FoodItemType.Bacon);

		crew.addItemToInventory(item1);
		crew.addItemToInventory(item2);
		crew.addItemToInventory(item3);
		assert (crew.getInventory().size() == 3);

		// Remove one and check its gone
		crew.removeItemFromInventory(item1);
		assert (crew.getInventory().size() == 2);
		assert (crew.getDistinctItems().size() == 1);
		assert (crew.getInventory().contains(item1) == false);

		// Add it back
		crew.addItemToInventory(item1);
		assert (crew.getInventory().size() == 3);

		// Check we only have two distinct items
		assert (crew.getDistinctItems().size() == 2);
	}

	@Test
	public void testGetFreeCrewMembers() {
		// Add four crew members
		for (int i = 1; i <= 4; i++) {
			game.addCrewMemeber("Josh " + i, CrewMemberTypes.Mechanic);
		}
		assert (crew.getFreeCrewMembers().size() == 4 && crew.freeCrewMembers() == 4);

		// use up actions of one
		crew.getCrewMember(0).sleep();
		crew.getCrewMember(0).sleep();
		assert (crew.getFreeCrewMembers().size() == 3 && crew.freeCrewMembers() == 3);

		// move to next day so actions reset
		game.moveToNextDay();
		assert (crew.getFreeCrewMembers().size() == 4 && crew.freeCrewMembers() == 4);

	}
}
