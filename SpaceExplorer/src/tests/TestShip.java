package tests;

import org.junit.Before;
import org.junit.Test;
import main.*;
import main.CrewMember.CrewMemberTypes;

public class TestShip {
	Game game;
	Ship ship;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "shipo");
		ship = game.ship;
	}

	@Test
	public void testPilot() {
		Planet oldPlanet = game.planet;
		ship.pilot(new CrewMember(game.crew, "Josh1", CrewMemberTypes.Pilot),
				new CrewMember(game.crew, "Josh2", CrewMemberTypes.Plain));
		assert game.planet != oldPlanet;

		boolean asteroid = false;
		for (int i = 0; i < 20; i++) {
			String result = ship.pilot(new CrewMember(game.crew, "Josh1", CrewMemberTypes.Pilot),
					new CrewMember(game.crew, "Josh2", CrewMemberTypes.Plain));
			if (result.contains("asteroid")) {
				asteroid = true;
			}
		}
		assert asteroid;
		assert ship.getShieldHealth() != 100;
	}
}
