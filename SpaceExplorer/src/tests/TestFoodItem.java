package tests;

import org.junit.Test;

import main.FoodItem;
import main.FoodItem.FoodItemType;
import main.Item.ItemType;

public class TestFoodItem {

	@Test
	public void testGetItemType() {
		assert new FoodItem(FoodItemType.Bacon).getItemType() == ItemType.Food;
	}
}
