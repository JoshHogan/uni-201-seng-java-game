package tests;

import org.junit.Before;
import org.junit.Test;

import main.Item.ItemType;
import main.MoneyBag;

public class TestMoneyBag {

	MoneyBag moneyBag;

	@Before
	public void setUp() throws Exception {
		moneyBag = new MoneyBag(10);
	}

	@Test
	public void testGetItemType() {
		assert moneyBag.getItemType() == ItemType.MoneyBag;
	}

	@Test
	public void testPrice() {
		assert moneyBag.price() == 10;
		moneyBag = new MoneyBag(20);
		assert moneyBag.price() == 20;
	}

	@Test
	public void testToString() {
		assert moneyBag.toString().equals("Money Bag containing: $10");
	}

	@Test
	public void testClone() {
		assert moneyBag.clone().price() == moneyBag.price();
	}
}
