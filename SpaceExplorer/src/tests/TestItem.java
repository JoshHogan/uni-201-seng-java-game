package tests;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import main.Game;
import main.Item;
import main.MedicalItem;
import main.MedicalItem.MedicalItemType;

public class TestItem {

	Game game;
	Item item;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "Shipo");
		item = new MedicalItem(MedicalItemType.FullHeal);
	}

	@Test
	public void testToString() {
		// Test all items have unique names
		ArrayList<String> names = new ArrayList<String>();
		for (Item item2 : game.spaceOutpost.getInventory()) {
			assert names.contains(item2.toString()) == false;
			names.add(item2.toString());
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEqualsObject() {
		assert item.equals(game) == false;
		assert item.equals(null) == false;
		assert item.equals(new MedicalItem(MedicalItemType.HealthBoost)) == false;
		assert item.equals(new MedicalItem(MedicalItemType.FullHeal));

	}

}
