package tests;

import main.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class TestSpaceOutpost {

	Game game;
	SpaceOutpost so;
	Random rand;

	@Before
	public void setUp() throws Exception {
		game = new Game(4, "shipo");
		so = game.spaceOutpost;
		rand = new Random();
	}

	@Test
	public void testBuyInt() throws Exception {

		for (int i = 0; i < 100; i++) {
			assert so.buy(0);
			assert game.crew.getInventory().size() == 1;

			int j = 2;
			while (so.buy(rand.nextInt(so.getInventory().size()))) {
				assert game.crew.getInventory().size() == j;
				j++;
			}
			assert game.crew.bankAccount >= 0 && game.crew.bankAccount < 100;
			setUp();
		}
	}

	@Test
	public void testGetInventory() {
		assert so.getInventory().size() > 0;
		assert so.getInventory().get(0) instanceof Item;
	}

}
