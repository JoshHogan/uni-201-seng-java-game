package tests;

import org.junit.Test;

import main.Item;
import main.MedicalItem;
import main.MedicalItem.MedicalItemType;

public class TestMedicalItem {
	Item mi;

	@Test
	public void testToString() {
		mi = new MedicalItem(MedicalItemType.FullHeal);
		assert mi.price() == 60;
		assert mi.toString().equals("Full Health Potion");
		assert ((MedicalItem) mi).healAmount() == 100;

		mi = new MedicalItem(MedicalItemType.SpacePlagueCure);
		assert mi.price() == 100;
		assert mi.toString().equals("Space Plague Cure");
		assert ((MedicalItem) mi).healAmount() == 10;

		mi = new MedicalItem(MedicalItemType.HealthBoost);
		assert mi.price() == 25;
		assert mi.toString().equals("Health Booster 20%");
		assert ((MedicalItem) mi).healAmount() == 30;
	}
}
