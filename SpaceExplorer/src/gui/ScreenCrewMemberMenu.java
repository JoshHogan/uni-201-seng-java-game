package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import main.CrewMember;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.Font;

/**
 * This menu allows the user to see information about a crew member and select
 * an action for them
 */
public class ScreenCrewMemberMenu extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();
	private CrewMember crewMember;

	/**
	 * Create the application.
	 * 
	 * @param manager  the all encompassing window manager
	 * @param selected the selected crew member
	 */
	public ScreenCrewMemberMenu(WindowManager manager, CrewMember selected) {
		crewMember = selected;
		startWindow(manager, frame);
	}

	/**
	 * Handles that the user clicked a certain action button using a method in
	 * window manager
	 * 
	 * @param mode the type of action that the current crew member is to perform
	 */
	public void handleButtonPress(Modes mode) {
		manager.handleSelectedCrewMember((ScreenAbstractMenu) this, crewMember, mode);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 546, 308);
		frame.getContentPane().setLayout(null);

		JButton btnUseItem = new JButton("Use or Eat Item");
		btnUseItem.setFont(new Font("Dialog", Font.BOLD, 10));
		btnUseItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleButtonPress(Modes.UseItem);
			}
		});

		btnUseItem.setBounds(10, 228, 132, 23);
		frame.getContentPane().add(btnUseItem);

		JButton btnSleep = new JButton("Sleep");
		btnSleep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleButtonPress(Modes.Sleep);
			}
		});
		btnSleep.setBounds(144, 227, 89, 23);
		frame.getContentPane().add(btnSleep);

		JButton btnRepairShip = new JButton("Repair Ship");
		btnRepairShip.setFont(new Font("Dialog", Font.BOLD, 10));
		btnRepairShip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleButtonPress(Modes.RepairShip);
			}
		});
		btnRepairShip.setBounds(235, 227, 107, 23);
		frame.getContentPane().add(btnRepairShip);

		JButton btnPilotShip = new JButton("Pilot Ship");
		btnPilotShip.setFont(new Font("Dialog", Font.BOLD, 10));
		btnPilotShip.setEnabled(manager.game.crew.freeCrewMembers() > 1);
		btnPilotShip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleButtonPress(Modes.PilotShip1);
			}
		});
		btnPilotShip.setBounds(342, 227, 89, 23);
		frame.getContentPane().add(btnPilotShip);

		JButton btnExplore = new JButton("Explore");
		btnExplore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleButtonPress(Modes.Explore);
			}
		});
		btnExplore.setBounds(433, 227, 99, 23);
		frame.getContentPane().add(btnExplore);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goBackToMainMenu();
			}
		});
		btnGoBack.setBounds(10, 152, 89, 23);
		frame.getContentPane().add(btnGoBack);

		JLabel label = new JLabel(crewMember.getType());
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setOpaque(true);
		label.setForeground(Color.YELLOW);
		label.setBackground(Color.BLACK);
		label.setBounds(139, 125, 117, 16);
		frame.getContentPane().add(label);

		JLabel crewImageLabel = new JLabel("");
		crewImageLabel.setOpaque(true);
		crewImageLabel.setBackground(Color.YELLOW);
		crewImageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		crewImageLabel.setIcon(crewMember.getImageIcon());
		crewImageLabel.setBounds(139, 11, 117, 129);
		frame.getContentPane().add(crewImageLabel);

		JLabel crewHealthLabel = new JLabel("Heath: " + crewMember.health);
		crewHealthLabel.setOpaque(true);
		crewHealthLabel.setBackground(Color.BLACK);
		crewHealthLabel.setForeground(Color.YELLOW);
		crewHealthLabel.setBounds(10, 40, 117, 16);
		frame.getContentPane().add(crewHealthLabel);

		JLabel crewHungerLabel = new JLabel("Hunger: " + crewMember.hunger);
		crewHungerLabel.setOpaque(true);
		crewHungerLabel.setBackground(Color.BLACK);
		crewHungerLabel.setForeground(Color.YELLOW);
		crewHungerLabel.setBounds(10, 68, 117, 16);
		frame.getContentPane().add(crewHungerLabel);

		JLabel crewTirednessLabel = new JLabel("Tiredness: " + crewMember.tiredness);
		crewTirednessLabel.setOpaque(true);
		crewTirednessLabel.setBackground(Color.BLACK);
		crewTirednessLabel.setForeground(Color.YELLOW);
		crewTirednessLabel.setBounds(10, 96, 117, 16);
		frame.getContentPane().add(crewTirednessLabel);

		JLabel crewActionsLabel = new JLabel("Actions: " + crewMember.getNumberOfFreeActions());
		crewActionsLabel.setOpaque(true);
		crewActionsLabel.setBackground(Color.BLACK);
		crewActionsLabel.setForeground(Color.YELLOW);
		crewActionsLabel.setBounds(10, 124, 117, 16);
		frame.getContentPane().add(crewActionsLabel);

		JLabel crewNameLabel = new JLabel(crewMember.getName());
		crewNameLabel.setOpaque(true);
		crewNameLabel.setForeground(Color.YELLOW);
		crewNameLabel.setBackground(Color.BLACK);
		crewNameLabel.setBounds(10, 11, 117, 16);
		frame.getContentPane().add(crewNameLabel);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ScreenCrewMemberMenu.class.getResource("/img/mainBck.jpg")));
		lblNewLabel.setBounds(0, 0, 538, 289);
		frame.getContentPane().add(lblNewLabel);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
