package gui;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

/**
 * This window is a simple info box to the user which shows them some text
 */
public class ScreenInfoBox extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();
	String text;
	boolean goBackToMain;

	/**
	 * Create the application, with the default behavior of going back to the main
	 * menu
	 * 
	 * @param manager       the all encompassing window manager
	 * @param textToDisplay the text to display in this info-box
	 */
	public ScreenInfoBox(WindowManager manager, String textToDisplay) {
		this(manager, textToDisplay, true);
	}

	/**
	 * Create the application.
	 * 
	 * @param manager       the all encompassing window manager
	 * @param textToDisplay the text to display in this info-box
	 * @param goBackToMain  a boolean which if true this info-box will return to the
	 *                      main menu
	 */
	public ScreenInfoBox(WindowManager manager, String textToDisplay, boolean goBackToMain) {
		this.text = textToDisplay;
		this.goBackToMain = goBackToMain;
		startWindow(manager, frame);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 786, 230);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JTextArea txtArea = new JTextArea();
		txtArea.setLineWrap(true);
		txtArea.setEditable(false);
		txtArea.setText(text);
		txtArea.setBounds(10, 11, 764, 146);
		frame.getContentPane().add(txtArea);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Calls the function in manager that closes this window and opens new one
				if (goBackToMain)
					goBackToMainMenu();
				else
					closeWindow();
			}
		});
		btnGoBack.setBounds(682, 169, 92, 23);
		frame.getContentPane().add(btnGoBack);
		frame.getRootPane().setDefaultButton(btnGoBack);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(ScreenInfoBox.class.getResource("/img/mainBck.jpg")));
		lblNewLabel.setBounds(0, 0, 786, 284);
		frame.getContentPane().add(lblNewLabel);
	}
}
