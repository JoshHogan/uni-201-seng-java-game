package gui;

import java.util.ArrayList;

import main.CrewMember;
import main.FoodItem;
import main.Game;
import main.Item;
import main.MedicalItem;
import main.TransporterPart;

enum Modes {
	RepairShip, PilotShip1, PilotShip2, Sleep, UseItem, Explore
};

/*
 * Window manager class managers the GUI Screen classes and the order in which
 * they open
 */

public class WindowManager {
	/**
	 * Holds the game instance that contains all elements of the game
	 */
	public Game game;
	/**
	 * The mode for which action is going to be performed This is used if an action
	 * is selected before a crew member
	 */
	private Modes mode;
	/**
	 * The main menu screen which is launched once the game is setup
	 */
	private ScreenMain mainScreen;
	/**
	 * A temporary variable to hold a crew member who will be the first pilot of the
	 * ship
	 */
	private CrewMember tempPilot;

	/**
	 * Runs the program including the start introduction screens
	 * 
	 * @param args (not used)
	 */
	public static void main(String[] args) {
		WindowManager manager = new WindowManager();
		manager.launchStartScreen();

	}

	/**
	 * Launches the start screen which just has a big button to start the game
	 */
	public void launchStartScreen() {
		new ScreenStart(this);
	}

	/**
	 * Closes the first screen and opens a screen which allows the user to choose
	 * the name for the ship
	 * 
	 * @param startWindow the initial start window that it to be closed
	 */
	public void closeStartScreen(ScreenStart startWindow) {
		startWindow.closeWindow();
		new ScreenSetupShip(this);
	}

	/**
	 * Closes the ship setup window and opens up one to choose the crew
	 * 
	 * @param shipWindow   the ship setup window that is to be closed
	 * @param numberOfCrew the number of crew that the user chose with the slider
	 */
	public void closeShipSetupScreen(ScreenSetupShip shipWindow, int numberOfCrew) {
		shipWindow.closeWindow();
		new ScreenSetupCrew(this, numberOfCrew);
	}

	/**
	 * Closes the crew setup screen and opens the main menu ready for the game to
	 * start
	 * 
	 * @param shipWindow the ship setup window that is to be closed before the main
	 *                   menu is opened
	 */
	public void closeCrewSetupScreen(ScreenSetupCrew shipWindow) {
		shipWindow.closeWindow();
		launchMainMenuScreen();
	}

	// END OF INTRO Section where the game is set up
	// *******************************************************************************
	// *******************************************************************************
	// From here down handles the main running to the program

	/**
	 * This launches the main menu screen for the program but also checks to see if
	 * the game has ended
	 */
	public void launchMainMenuScreen() {
		// Check if the game is complete and if so end it
		if (game.gameComplete())
			launchEndScreen();
		else
			mainScreen = new ScreenMain(this);
	}

	/**
	 * This closes the main menu screen for the program, if it is not equal to null
	 */
	public void closeMainMenuScreen() {
		if (mainScreen != null)
			mainScreen.closeWindow();
		else
			System.out.println("Tried to close null MainScreen");
	}

	/**
	 * Closes the current window that is open, resets the mode and returns to the
	 * main menu screen
	 * 
	 * @param currentWindow the current window that is to be closed
	 */
	public void returnToMainScreen(ScreenAbstractMenu currentWindow) {
		currentWindow.closeWindow();
		mode = null;
		launchMainMenuScreen();
	}

	/**
	 * Launches the screen that has information about the ship
	 */
	public void launchShipMenuScreen() {
		closeMainMenuScreen();
		new ScreenShipMenu(this);
	}

	/**
	 * Launches the screen that has the actions for whichever crew member was
	 * clicked
	 * 
	 * @param crewMember the crew member that was clicked
	 */
	public void launchCrewMemberMenu(CrewMember crewMember) {
		closeMainMenuScreen();
		new ScreenCrewMemberMenu(this, crewMember);
	}

	/**
	 * If there are some actions left, launches the menu to confirm if the user
	 * wants to go to the next day otherwise just moves on to the next day without
	 * confirmation
	 */
	public void launchNextDayMenu() {
		closeMainMenuScreen();
		if (game.crew.freeCrewMembers() > 0)
			new ScreenNextDayConfirm(this);
		else
			goToNextDay();
	}

	/**
	 * Launches the screen that allows the user to select items to buy from the
	 * outpost
	 */
	public void launchSpaceOutpostScreen() {
		closeMainMenuScreen();
		new ScreenSpaceOutpost(this);
	}

	/**
	 * Moves the game on to the next day and displays anything that happened in an
	 * info box
	 */
	public void goToNextDay() {
		new ScreenInfoBox(this, game.moveToNextDay());
	}

	/**
	 * Sets the current mode to repair ship mode and launches a menu that lets the
	 * user pick a crew member to repair the ship
	 */
	public void repairShip() {
		mode = Modes.RepairShip;
		new ScreenCrewMemberSelect(this, game.crew.getFreeCrewMembers());
	}

	/**
	 * Sets the current mode to pilot ship mode and launches a menu that lets the
	 * user pick a crew member to be the first pilot
	 */
	public void pilotShip() {
		mode = Modes.PilotShip1;
		new ScreenCrewMemberSelect(this, game.crew.getFreeCrewMembers());
	}

	/**
	 * Handles a selected crew member who has been given an action to perform by the
	 * selected Mode that is also given
	 * 
	 * @param window   the previous window that is to be closed
	 * @param selected the selected crew member who is going to do this action
	 * @param setMode  the mode or what the action actually is, for example
	 *                 Modes.RepairShip
	 */
	public void handleSelectedCrewMember(ScreenAbstractMenu window, CrewMember selected, Modes setMode) {
		mode = setMode;
		handleSelectedCrewMember(window, selected);
	}

	/**
	 * Handles a selected crew member who has been given an action to perform, the
	 * action is given by the mode that has previously been selected
	 * 
	 * @param window   the previous window that is to be closed
	 * @param selected the selected crew member who is going to do this action
	 */
	public void handleSelectedCrewMember(ScreenAbstractMenu window, CrewMember selected) {
		// Close current opened window
		window.closeWindow();

		// Switch case depending what mode we are in
		switch (mode) {
		case RepairShip:
			new ScreenInfoBox(this, selected.repairShip());
			break;
		case PilotShip1:
			// We still need to select the second pilot, so store the first one
			tempPilot = selected;

			ArrayList<CrewMember> freeCrewMembers = game.crew.getFreeCrewMembers();
			freeCrewMembers.remove(selected);

			mode = Modes.PilotShip2;
			new ScreenCrewMemberSelect(this, freeCrewMembers);
			break;
		case PilotShip2:
			new ScreenInfoBox(this, game.ship.pilot(selected, tempPilot));
			break;
		case Explore:
			new ScreenInfoBox(this, selected.explorePlanet());
			break;
		case Sleep:
			new ScreenInfoBox(this, selected.sleep());
			break;
		case UseItem:
			new ScreenInventoryItemSelect(this, selected);
			break;
		default:
			launchMainMenuScreen();
			break;
		}
	}

	/**
	 * Buys all of the items given in the provided list and shows a info box of what
	 * was brought
	 * 
	 * @param itemsToBuy a list containing the items to buy
	 */
	public void buyItems(ArrayList<Item> itemsToBuy) {
		String text = "You Brought: \n";
		for (Item item : itemsToBuy) {
			game.spaceOutpost.buy(item);
			text += item.toString() + "\n";
		}
		new ScreenInfoBox(this, text);
	}

	/**
	 * This handles when a certain crew member is selected to use or eat a specific
	 * item
	 * 
	 * @param window   the previous window that is to be closed
	 * @param item     the item that is going to be used or eaten
	 * @param selected the selected crewMember who is going to use or eat the item
	 */
	public void handleSelectedItem(ScreenInventoryItemSelect window, Item item, CrewMember selected) {
		window.closeWindow();
		// Use the item that the user picked for the selected crewMember
		if (item instanceof TransporterPart) {
			new ScreenInfoBox(this, "You cannot consume a transporter part you egg");
		} else if (item instanceof FoodItem) {
			new ScreenInfoBox(this, selected.eat((FoodItem) item));
		} else if (item instanceof MedicalItem) {
			new ScreenInfoBox(this, selected.heal((MedicalItem) item));
		}
	}

	/**
	 * Launches the final end game screen which tells the user if they won or lost
	 */
	public void launchEndScreen() {
		new ScreenEnd(this);
	}

	/**
	 * Launches a new game if the user clicks the new game button
	 */
	public void newGameClick() {
		launchStartScreen();
	}

}
