package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This window shows to the user whether they have won the game or not
 */
public class ScreenEnd extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();

	/*
	 * Create the window
	 */
	public ScreenEnd(WindowManager incomingManager) {
		startWindow(incomingManager, frame);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblTheEnd = new JLabel("");
		lblTheEnd.setOpaque(true);
		lblTheEnd.setBackground(Color.BLACK);
		lblTheEnd.setForeground(Color.YELLOW);
		lblTheEnd.setFont(new Font("Arial", Font.BOLD, 23));
		lblTheEnd.setHorizontalAlignment(SwingConstants.CENTER);
		lblTheEnd.setBounds(0, 60, 450, 51);
		frame.getContentPane().add(lblTheEnd);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel.setForeground(Color.YELLOW);
		lblNewLabel.setBounds(0, 144, 450, 30);
		frame.getContentPane().add(lblNewLabel);

		/*
		 * Checks if the current day is greater than the total amount of days. Ends the
		 * game if this is true, otherwise, game has been won
		 */

		if (manager.game.getCurrentDay() > manager.game.getNumberOfDays()) {
			lblTheEnd.setText("Oh No! You ran out of time!");
			lblNewLabel.setText("Please press the button below to try again");
		}

		else {
			lblTheEnd.setText("You Win! Great Job!");
			lblNewLabel.setText("Please press the button below to play again");
		}

		/*
		 * Closes window and begins a new game
		 */
		JButton btnNewButton = new JButton("NEW GAME");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeWindow();
				manager.newGameClick();
			}
		});
		btnNewButton.setBounds(167, 207, 117, 29);
		frame.getContentPane().add(btnNewButton);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ScreenEnd.class.getResource("/img/mainBck.jpg")));
		label.setBounds(0, 0, 450, 278);
		frame.getContentPane().add(label);
	}
}
