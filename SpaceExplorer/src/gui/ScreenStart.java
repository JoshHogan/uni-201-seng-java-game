package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.ImageIcon;

/**
 * The initial start screen which just has a big button labeled start on it
 */
public class ScreenStart extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();

	public ScreenStart(WindowManager incomingManager) {
		startWindow(incomingManager, frame);
	}

	/**
	 * Closes this screen and opens the first setup screen
	 */
	public void finishedWindow() {
		manager.closeStartScreen(this);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel titleLabel = new JLabel("Space Explorer");
		titleLabel.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 27));
		titleLabel.setOpaque(true);
		// the background color is used only if the component is opaque

		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setBackground(Color.BLACK);
		titleLabel.setForeground(Color.YELLOW);
		titleLabel.setBounds(0, 63, 450, 66);
		frame.getContentPane().add(titleLabel);

		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				finishedWindow();
			}
		});
		btnStart.setBounds(164, 160, 117, 29);
		frame.getContentPane().add(btnStart);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ScreenStart.class.getResource("/img/mainBck.jpg")));
		lblNewLabel.setBounds(0, 0, 450, 278);
		frame.getContentPane().add(lblNewLabel);
	}
}
