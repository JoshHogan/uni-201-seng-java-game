package gui;

import javax.swing.JFrame;

/**
 * An abstract class which is inherited by all of the other screens in this
 * application Has some very simple methods such as starting and closing the
 * window, and also returning to the main screen
 */
public abstract class ScreenAbstractMenu {

	protected WindowManager manager;
	private JFrame frame;

	/**
	 * Create the application, initialize and set frame visible to true
	 * 
	 * @param manager the all encompassing window manager
	 * @param frame   the frame for this window
	 */
	public void startWindow(WindowManager manager, JFrame frame) {
		this.manager = manager;
		this.frame = frame;

		initialize();
		frame.setVisible(true);
	}

	/**
	 * Closes the window by calling the frame.dispose method
	 */
	public void closeWindow() {
		frame.dispose();
	}

	/**
	 * Returns back to the main menu screen, the window is also closed
	 */
	public void goBackToMainMenu() {
		manager.returnToMainScreen(this);
	}

	/**
	 * Initialize the contents of the frame, and all components
	 */
	protected abstract void initialize();
}
