package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;

import main.CrewMember.CrewMemberTypes;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;

/**
 * One of the setup screens that allows the user to choose a name and a type for
 * each of the crew members
 */
public class ScreenSetupCrew extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();

	private JLabel selectedCrewTypeLabel;
	private CrewMemberTypes type;
	private int maxCrew;

	/**
	 * Create the application window
	 * 
	 * @param incomingManager the all encompassing window manager
	 * @param maxCrew         the amount of crew members that the user will choose
	 */
	public ScreenSetupCrew(WindowManager incomingManager, int maxCrew) {
		this.maxCrew = maxCrew;
		startWindow(incomingManager, frame);

		changeSelectedLabel("Plain");
		type = CrewMemberTypes.Plain;
	}

	/**
	 * Launches the next screen after closing this one
	 */
	public void finishedWindow() {
		manager.closeCrewSetupScreen(this);
	}

	/**
	 * Changes the text of the crew type label on the form
	 * 
	 * @param crewType the text to change the label to
	 */
	public void changeSelectedLabel(String crewType) {
		selectedCrewTypeLabel.setText("Crew Member Type Selected: " + crewType);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 457, 311);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel crewTitle = new JLabel("Crew Creation");
		crewTitle.setHorizontalAlignment(SwingConstants.CENTER);
		crewTitle.setBackground(Color.BLACK);
		crewTitle.setOpaque(true);
		crewTitle.setForeground(Color.YELLOW);
		crewTitle.setBounds(0, 6, 450, 28);
		frame.getContentPane().add(crewTitle);

		JLabel crewNameLbl = new JLabel("  Crew member name:");
		crewNameLbl.setOpaque(true);
		crewNameLbl.setBackground(Color.BLACK);
		crewNameLbl.setForeground(Color.YELLOW);
		crewNameLbl.setBounds(0, 46, 188, 27);
		frame.getContentPane().add(crewNameLbl);

		final JTextField inputCrewName = new JTextField();
		inputCrewName.setBounds(193, 47, 245, 26);
		frame.getContentPane().add(inputCrewName);
		inputCrewName.setColumns(10);

		JLabel crewTitleLabel = new JLabel("Crew Member Type (Hover over each button to see info):");
		crewTitleLabel.setOpaque(true);
		crewTitleLabel.setBackground(Color.BLACK);
		crewTitleLabel.setForeground(Color.YELLOW);
		crewTitleLabel.setBounds(0, 87, 457, 26);
		frame.getContentPane().add(crewTitleLabel);

		JButton typeOne = new JButton("Explorer");
		typeOne.setToolTipText("Explorer loses no energy while exploring");
		typeOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				type = CrewMemberTypes.Explorer;
				changeSelectedLabel("Explorer");
			}
		});
		typeOne.setBounds(10, 125, 117, 29);
		frame.getContentPane().add(typeOne);

		JButton typeTwo = new JButton("Mechanic");
		typeTwo.setToolTipText("Mechanic has increased ship repair abilities");
		typeTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				type = CrewMemberTypes.Mechanic;
				changeSelectedLabel("Mechanic");
			}
		});
		typeTwo.setBounds(161, 125, 117, 29);
		frame.getContentPane().add(typeTwo);

		JButton typeThree = new JButton("Plain (Hard)");
		typeThree.setToolTipText("No buffs (Select for a challenge))");
		typeThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSelectedLabel("Plain");
				type = CrewMemberTypes.Plain;
			}
		});
		typeThree.setBounds(304, 125, 117, 29);
		frame.getContentPane().add(typeThree);

		JButton typeFour = new JButton("Extra Health");
		typeFour.setFont(new Font("Dialog", Font.BOLD, 11));
		typeFour.setToolTipText("Gives crew member +30%  max health");
		typeFour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSelectedLabel("Extra Health");
				type = CrewMemberTypes.ExtraHealth;
			}
		});
		typeFour.setBounds(10, 181, 117, 29);
		frame.getContentPane().add(typeFour);

		JButton typeFive = new JButton("Pilot");
		typeFive.setToolTipText("Pilot loses no energy while flying the ship");
		typeFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSelectedLabel("Pilot");
				type = CrewMemberTypes.Pilot;
			}
		});
		typeFive.setBounds(161, 181, 117, 29);
		frame.getContentPane().add(typeFive);

		JButton typeSix = new JButton("Sleeper");
		typeSix.setToolTipText("Sleeper has boosted energy regeneration when sleeping");
		typeSix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSelectedLabel("Sleeper");
				type = CrewMemberTypes.Sleeper;
			}
		});
		typeSix.setBounds(304, 181, 117, 29);
		frame.getContentPane().add(typeSix);

		JButton btnNext = new JButton("CONFIRM");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manager.game.addCrewMemeber(inputCrewName.getText(), type);
				// Reset everything
				changeSelectedLabel("Plain");
				type = CrewMemberTypes.Plain;
				inputCrewName.setText("");

				if (maxCrew == manager.game.crew.numberOfCrew()) { // checks if this last crew member
					finishedWindow();
				}
			}
		});

		btnNext.setBounds(321, 242, 117, 29);
		frame.getContentPane().add(btnNext);

		selectedCrewTypeLabel = new JLabel("Crew Member Type Selected: ");
		selectedCrewTypeLabel.setForeground(Color.YELLOW);
		selectedCrewTypeLabel.setBounds(32, 248, 296, 16);
		frame.getContentPane().add(selectedCrewTypeLabel);

		JLabel backgroundImgLabel = new JLabel("");
		backgroundImgLabel.setIcon(new ImageIcon(ScreenSetupCrew.class.getResource("/img/mainBck.jpg")));
		backgroundImgLabel.setBounds(0, 0, 457, 283);
		frame.getContentPane().add(backgroundImgLabel);

	}

}
