package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import main.CrewMember;
import main.Game;
import main.CrewMember.CrewMemberTypes;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;

/**
 * This is the main menu screen that has information about the crew and also
 * buttons to open up the other menu screens
 */
public class ScreenMain extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();

	/**
	 * Create the application.
	 * 
	 * @param manager the all encompassing window manager
	 */
	public ScreenMain(WindowManager manager) {
		startWindow(manager, frame);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 900, 534);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel planetTitleLabel = new JLabel("You have arrived at the planet: " + manager.game.planet.getName());
		planetTitleLabel.setFont(new Font("Arial", Font.PLAIN, 27));
		planetTitleLabel.setOpaque(true);
		planetTitleLabel.setBackground(Color.BLACK);
		planetTitleLabel.setForeground(Color.YELLOW);
		planetTitleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		planetTitleLabel.setBounds(0, 0, 900, 64);
		frame.getContentPane().add(planetTitleLabel);

		JLabel shipNameLabel = new JLabel(manager.game.ship.getName());
		shipNameLabel.setForeground(Color.YELLOW);
		shipNameLabel.setFont(new Font("Arial", Font.PLAIN, 20));
		shipNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		shipNameLabel.setBounds(43, 369, 159, 29);
		frame.getContentPane().add(shipNameLabel);

		JButton shopButton = new JButton("SHOP");
		shopButton.setBounds(80, 76, 81, 29);
		shopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Close this window and opens the shop
				manager.launchSpaceOutpostScreen();
			}
		});
		frame.getContentPane().add(shopButton);

		JButton shipControlsButton = new JButton("Ship");
		shipControlsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Open the ship menu and close this one
				manager.launchShipMenuScreen();
			}
		});
		shipControlsButton.setBounds(80, 323, 81, 25);
		frame.getContentPane().add(shipControlsButton);

		JButton nextDayButton = new JButton("Next day");
		nextDayButton.setFont(new Font("Arial", Font.PLAIN, 13));
		nextDayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Close this window and open the next day menu
				manager.launchNextDayMenu();
			}
		});
		nextDayButton.setBounds(780, 419, 114, 25);
		frame.getContentPane().add(nextDayButton);

		JLabel shipTerminalTitleLabel = new JLabel("SHIP TERMINAL");
		shipTerminalTitleLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		shipTerminalTitleLabel.setOpaque(true);
		shipTerminalTitleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		shipTerminalTitleLabel.setForeground(Color.YELLOW);
		shipTerminalTitleLabel.setBackground(Color.BLACK);
		shipTerminalTitleLabel.setBounds(0, 410, 900, 40);
		frame.getContentPane().add(shipTerminalTitleLabel);

		JLabel daysPlayedLabel = new JLabel(
				"Days Searching: " + manager.game.getCurrentDay() + "/" + manager.game.getNumberOfDays());
		daysPlayedLabel.setFont(new Font("Arial", Font.PLAIN, 30));
		daysPlayedLabel.setForeground(Color.YELLOW);
		daysPlayedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		daysPlayedLabel.setBounds(43, 455, 366, 47);
		frame.getContentPane().add(daysPlayedLabel);

		JLabel shipImage = new JLabel("");
		shipImage.setIcon(new ImageIcon(ScreenMain.class.getResource("/img/rocketShip1.png")));
		shipImage.setHorizontalAlignment(SwingConstants.CENTER);
		shipImage.setBounds(43, 94, 159, 237);
		frame.getContentPane().add(shipImage);

		// This holds the x,y offsets for the different crew member sections
		final int offsets[][] = { { 0, 0 }, { 296, 0 }, { 0, 167 }, { 296, 167 } };

		// This loop runs through each crew member

		int i = 0;

		// Uncomment this to get the screen working, comment it out to change cosmetic
		// stuff
		for (i = 0; i < manager.game.crew.numberOfCrew(); i++) {
			CrewMember selected = manager.game.crew.getCrewMember(i);

			JButton crewButton = new JButton(selected.getName());
			crewButton.setToolTipText(crewButton.getText());
			// This is used to identify the index of the crew members
			crewButton.setMnemonic(i);
			crewButton.setEnabled(selected.freeAction());
			crewButton.setBounds(273 + offsets[i][0], 94 + offsets[i][1], 117, 29);
			crewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int index = (((JButton) e.getSource()).getMnemonic());
					manager.launchCrewMemberMenu(manager.game.crew.getCrewMember(index));
				}
			});

			frame.getContentPane().add(crewButton);

			JLabel crewTypeLabel = new JLabel(selected.getType());
			crewTypeLabel.setHorizontalAlignment(SwingConstants.CENTER);
			crewTypeLabel.setOpaque(true);
			crewTypeLabel.setForeground(selected.hasSpacePlague ? Color.RED : Color.YELLOW);
			crewTypeLabel.setBackground(Color.BLACK);
			crewTypeLabel.setBounds(402 + offsets[i][0], 212 + offsets[i][1], 117, 16);
			frame.getContentPane().add(crewTypeLabel);

			JLabel crewImageLabel = new JLabel("");
			crewImageLabel.setOpaque(true);
			crewImageLabel.setBackground(Color.YELLOW);
			crewImageLabel.setHorizontalAlignment(SwingConstants.CENTER);
			crewImageLabel.setIcon(selected.getImageIcon());
			crewImageLabel.setBounds(402 + offsets[i][0], 94 + offsets[i][1], 117, 134);
			frame.getContentPane().add(crewImageLabel);

			JLabel crewHealthLabel = new JLabel("Heath: " + selected.health);
			crewHealthLabel.setOpaque(true);
			crewHealthLabel.setBackground(Color.BLACK);
			crewHealthLabel.setForeground(Color.YELLOW);
			crewHealthLabel.setBounds(273 + offsets[i][0], 128 + offsets[i][1], 117, 16);
			frame.getContentPane().add(crewHealthLabel);

			JLabel crewHungerLabel = new JLabel("Hunger: " + selected.hunger);
			crewHungerLabel.setOpaque(true);
			crewHungerLabel.setBackground(Color.BLACK);
			crewHungerLabel.setForeground(Color.YELLOW);
			crewHungerLabel.setBounds(273 + offsets[i][0], 156 + offsets[i][1], 117, 16);
			frame.getContentPane().add(crewHungerLabel);

			JLabel crewTirednessLabel = new JLabel("Tiredness: " + selected.tiredness);
			crewTirednessLabel.setOpaque(true);
			crewTirednessLabel.setBackground(Color.BLACK);
			crewTirednessLabel.setForeground(Color.YELLOW);
			crewTirednessLabel.setBounds(273 + offsets[i][0], 184 + offsets[i][1], 117, 16);
			frame.getContentPane().add(crewTirednessLabel);

			JLabel crewActionsLabel = new JLabel("Actions: " + selected.getNumberOfFreeActions());
			crewActionsLabel.setOpaque(true);
			crewActionsLabel.setBackground(Color.BLACK);
			crewActionsLabel.setForeground(Color.YELLOW);
			crewActionsLabel.setBounds(273 + offsets[i][0], 212 + offsets[i][1], 117, 16);
			frame.getContentPane().add(crewActionsLabel);
			// Uncomment this to finish the 'for' loop

		}

		JLabel lineLabel = new JLabel("");
		lineLabel.setOpaque(true);
		lineLabel.setBackground(Color.BLACK);
		lineLabel.setBounds(449, 441, 6, 82);
		frame.getContentPane().add(lineLabel);

		JLabel partsCollectedLabel = new JLabel(
				"Parts Collected: " + manager.game.getPartsFound() + "/" + manager.game.getNumberOfParts());
		partsCollectedLabel.setFont(new Font("Arial", Font.PLAIN, 30));
		partsCollectedLabel.setForeground(Color.YELLOW);
		partsCollectedLabel.setBounds(550, 456, 326, 44);
		frame.getContentPane().add(partsCollectedLabel);
		JLabel backgroundImageLabel = new JLabel("bck");
		backgroundImageLabel.setIcon(new ImageIcon(ScreenMain.class.getResource("/img/mainBck.jpg")));
		backgroundImageLabel.setBounds(0, 0, 900, 576);
		frame.getContentPane().add(backgroundImageLabel);

	}

	public static void main(String[] args) {
		WindowManager managerStatic = new WindowManager();

		managerStatic.game = new Game(2, "Coolio shipo");
		managerStatic.game.addCrewMemeber("Crew Member Uno", CrewMemberTypes.Pilot);
		managerStatic.game.addCrewMemeber("Crew member Dos", CrewMemberTypes.Explorer);
		managerStatic.game.addCrewMemeber("Crew member 3", CrewMemberTypes.ExtraHealth);
		managerStatic.launchMainMenuScreen();
	}
}
