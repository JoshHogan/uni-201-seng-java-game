package gui;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;

/**
 * This is a simple dialogue box that confirms that the user wants to go to the
 * next day if there are still actions left
 */
public class ScreenNextDayConfirm extends ScreenAbstractMenu {

	JFrame frame = new JFrame();

	/**
	 * Create the window
	 * 
	 * @param manager the all encompassing window manager
	 */
	public ScreenNextDayConfirm(WindowManager manager) {
		startWindow(manager, frame);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 366, 203);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnConfirmNextDay = new JButton("Confirm Go To Next Day");
		btnConfirmNextDay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeWindow();
				manager.goToNextDay();
			}
		});
		btnConfirmNextDay.setBounds(22, 138, 209, 23);
		frame.getContentPane().add(btnConfirmNextDay);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Go back to the main menu
				goBackToMainMenu();
			}
		});
		btnGoBack.setBounds(243, 138, 89, 23);
		frame.getContentPane().add(btnGoBack);

		JLabel lblSomeCrewMembers = new JLabel("Some Crew Members still have actions remaining");
		lblSomeCrewMembers.setForeground(Color.YELLOW);
		lblSomeCrewMembers.setHorizontalAlignment(SwingConstants.CENTER);
		lblSomeCrewMembers.setBounds(0, 79, 366, 23);
		frame.getContentPane().add(lblSomeCrewMembers);

		JLabel lblAreYouSure = new JLabel("Are you sure you want to move to the next day?");
		lblAreYouSure.setForeground(Color.YELLOW);
		lblAreYouSure.setHorizontalAlignment(SwingConstants.CENTER);
		lblAreYouSure.setBounds(0, 102, 366, 14);
		frame.getContentPane().add(lblAreYouSure);

		JLabel lblStop = new JLabel("Stop!");
		lblStop.setOpaque(true);
		lblStop.setFont(new Font("Arial", Font.PLAIN, 30));
		lblStop.setHorizontalAlignment(SwingConstants.CENTER);
		lblStop.setBackground(Color.BLACK);
		lblStop.setForeground(Color.YELLOW);
		lblStop.setBounds(0, 22, 366, 45);
		frame.getContentPane().add(lblStop);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ScreenNextDayConfirm.class.getResource("/img/mainBck.jpg")));
		label.setBounds(0, -13, 389, 211);
		frame.getContentPane().add(label);
	}

}
