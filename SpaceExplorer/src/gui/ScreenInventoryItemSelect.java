package gui;

import java.awt.Color;
import java.awt.Font;
import java.util.Collections;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import main.CrewMember;
import main.Item;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This allows the user to choose an item from the inventory for the selected
 * crew member to either eat or use
 */
public class ScreenInventoryItemSelect extends ScreenAbstractMenu {

	CrewMember selected;
	private JFrame frame = new JFrame();
	ButtonGroup buttonGroup = new ButtonGroup();

	public ScreenInventoryItemSelect(WindowManager manager, CrewMember selected) {
		this.selected = selected;
		startWindow(manager, frame);
	}

	private void handleSelected(int mnemonic) {
		Item item = (Item) manager.game.crew.getDistinctItems().toArray()[mnemonic];
		manager.handleSelectedItem(this, item, selected);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setBounds(100, 100, 368, 465);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goBackToMainMenu();
			}
		});
		btnGoBack.setBounds(253, 403, 89, 23);
		frame.getContentPane().add(btnGoBack);

		JButton btnUseOrEat = new JButton("Use or Eat");
		btnUseOrEat.setEnabled(manager.game.crew.getInventory().size() > 0);
		btnUseOrEat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleSelected(buttonGroup.getSelection().getMnemonic());
			}
		});
		btnUseOrEat.setBounds(10, 403, 116, 23);
		frame.getContentPane().add(btnUseOrEat);

		int offset = 56;
		int i = 0;
		for (Item item : manager.game.crew.getDistinctItems()) {
			int count = Collections.frequency(manager.game.crew.getInventory(), item);
			JRadioButton rdbtnItem = new JRadioButton(Integer.toString(count) + "x  " + item.toString());
			rdbtnItem.setBackground(Color.BLACK);
			rdbtnItem.setForeground(Color.YELLOW);
			rdbtnItem.setMnemonic(i);
			rdbtnItem.setOpaque(true);
			rdbtnItem.setBounds(10, offset + i * 22, 332, 23);
			if (i == 0) {
				rdbtnItem.setSelected(true);
			}
			frame.getContentPane().add(rdbtnItem);
			buttonGroup.add(rdbtnItem);
			i++;
		}

		JLabel lblTitle = new JLabel("Inventory");
		lblTitle.setOpaque(true);
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setForeground(Color.YELLOW);
		lblTitle.setFont(new Font("Arial", Font.PLAIN, 30));
		lblTitle.setBackground(Color.BLACK);
		lblTitle.setBounds(0, 0, 352, 49);
		frame.getContentPane().add(lblTitle);

		JLabel backgroundImageLabel = new JLabel("bck");
		backgroundImageLabel.setIcon(new ImageIcon(ScreenMain.class.getResource("/img/mainBck.jpg")));
		backgroundImageLabel.setBounds(0, 0, 860, 444);
		frame.getContentPane().add(backgroundImageLabel);

	}

}
