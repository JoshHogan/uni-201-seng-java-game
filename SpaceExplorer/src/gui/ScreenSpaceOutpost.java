package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import main.Item;

import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 */

/**
 * This is a menu that allows the user to see what they have in their inventory
 * and also to buy items from the space outpost
 */
public class ScreenSpaceOutpost extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();
	private JTable table;
	/**
	 * This array-list holds an array of objects, the first index is always a
	 * spinner object and the second is the matching item
	 */
	private ArrayList<Object[]> spinnerAndItems = new ArrayList<Object[]>();
	private JLabel lblTotalPrice;

	public ScreenSpaceOutpost(WindowManager manager) {
		startWindow(manager, frame);

	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setBounds(100, 100, 610, 468);
		table = new JTable();

		DefaultTableModel tableModel = new DefaultTableModel(new Object[][] {}, new String[] { "Count", "Item Name" });
		table.setModel(tableModel);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(44);
		table.getColumnModel().getColumn(0).setMinWidth(43);
		table.getColumnModel().getColumn(0).setMaxWidth(44);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.setBounds(332, 85, 258, 311);

		tableModel.addRow(new String[] { "Count", "Item Name" });
		// Add all of the items to the table
		for (Item item : manager.game.crew.getDistinctItems()) {
			int count = Collections.frequency(manager.game.crew.getInventory(), item);
			tableModel.addRow(new String[] { Integer.toString(count), item.toString() });
		}

		frame.getContentPane().add(table);

		JLabel lblInventory = new JLabel("Inventory");
		lblInventory.setOpaque(true);
		lblInventory.setHorizontalAlignment(SwingConstants.CENTER);
		lblInventory.setForeground(Color.YELLOW);
		lblInventory.setFont(new Font("Arial", Font.PLAIN, 30));
		lblInventory.setBackground(Color.BLACK);
		lblInventory.setBounds(332, 11, 258, 47);
		frame.getContentPane().add(lblInventory);

		JLabel lblSpaceOutpost = new JLabel("Space Outpost");
		lblSpaceOutpost.setOpaque(true);
		lblSpaceOutpost.setHorizontalAlignment(SwingConstants.CENTER);
		lblSpaceOutpost.setForeground(Color.YELLOW);
		lblSpaceOutpost.setFont(new Font("Arial", Font.PLAIN, 30));
		lblSpaceOutpost.setBackground(Color.BLACK);
		lblSpaceOutpost.setBounds(10, 11, 258, 47);
		frame.getContentPane().add(lblSpaceOutpost);

		int offset = 72;
		// Item item = new MedicalItem(MedicalItemType.HealthBoost);

		for (Item item : manager.game.spaceOutpost.getInventory()) {
			JSpinner spinner = new JSpinner();
			spinner.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					updateTotalPrice();
				}
			});
			spinner.setModel(new SpinnerNumberModel(0, 0, null, 1));
			spinner.setBounds(20, offset - 3, 35, 20);
			((DefaultEditor) spinner.getEditor()).getTextField().setEditable(false);
			frame.getContentPane().add(spinner);

			JLabel lblItemName = new JLabel(item.toString());
			lblItemName.setFont(new Font("Tahoma", Font.PLAIN, 11));
			lblItemName.setForeground(Color.YELLOW);
			lblItemName.setBackground(Color.BLACK);
			lblItemName.setOpaque(true);
			lblItemName.setBounds(90, offset, 178, 14);
			frame.getContentPane().add(lblItemName);

			JLabel lblPrice = new JLabel("$" + Integer.toString(item.price()));
			lblPrice.setForeground(Color.YELLOW);
			lblPrice.setBackground(Color.BLACK);
			lblPrice.setOpaque(true);
			lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 11));
			lblPrice.setBounds(60, offset, 48, 14);
			frame.getContentPane().add(lblPrice);

			Object[] itemAndCount = { spinner, item };
			spinnerAndItems.add(itemAndCount);
			offset += 26;
		}

		JButton btnBuy = new JButton("Buy");
		btnBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buyItems();
			}
		});
		btnBuy.setBounds(206, 407, 89, 23);
		frame.getContentPane().add(btnBuy);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goBackToMainMenu();
			}
		});
		btnGoBack.setBounds(501, 407, 89, 23);
		frame.getContentPane().add(btnGoBack);

		lblTotalPrice = new JLabel("Total Price: $0");
		lblTotalPrice.setOpaque(true);
		lblTotalPrice.setForeground(Color.YELLOW);
		lblTotalPrice.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblTotalPrice.setBackground(Color.BLACK);
		lblTotalPrice.setBounds(10, 411, 100, 14);
		frame.getContentPane().add(lblTotalPrice);

		JLabel lblBalance = new JLabel("Balance: $" + Integer.toString(manager.game.crew.bankAccount));
		lblBalance.setOpaque(true);
		lblBalance.setForeground(Color.YELLOW);
		lblBalance.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblBalance.setBackground(Color.BLACK);
		lblBalance.setBounds(332, 56, 100, 14);
		frame.getContentPane().add(lblBalance);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ScreenSpaceOutpost.class.getResource("/img/mainBck.jpg")));
		label.setBounds(0, 0, 610, 445);
		frame.getContentPane().add(label);
	}

	protected void buyItems() {
		updateTotalPrice();
		int price = Integer.valueOf(lblTotalPrice.getText().substring(lblTotalPrice.getText().indexOf('$') + 1));

		if (price > manager.game.crew.bankAccount) {
			new ScreenInfoBox(manager, "You Cannot Purchase All of those Items", false);
		} else {
			ArrayList<Item> itemsToBuy = new ArrayList<Item>();

			for (Object[] spinnerAndItem : spinnerAndItems) {
				JSpinner spinner = (JSpinner) spinnerAndItem[0];
				Item item = (Item) spinnerAndItem[1];

				// Loop for as many times the user selected this item
				for (int i = 0; i < (Integer) spinner.getValue(); i++) {
					itemsToBuy.add(item);
				}
			}
			manager.buyItems(itemsToBuy);
			closeWindow();
		}

	}

	protected void updateTotalPrice() {
		String text = "Total Price: $";
		int price = 0;

		for (Object[] spinnerAndItem : spinnerAndItems) {
			JSpinner spinner = (JSpinner) spinnerAndItem[0];
			Item item = (Item) spinnerAndItem[1];

			price += (Integer) spinner.getValue() * item.price();
		}
		lblTotalPrice.setText(text + Integer.toString(price));
		if (price > manager.game.crew.bankAccount)
			lblTotalPrice.setForeground(Color.RED);
		else
			lblTotalPrice.setForeground(Color.YELLOW);
	}
}
