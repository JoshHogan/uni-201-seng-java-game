package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

import main.CrewMember;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

/**
 * This allows the user to select a crew member from the list of crew members,
 * is used to choose who is going to pilot the ship
 */
public class ScreenCrewMemberSelect extends ScreenAbstractMenu {

	private ArrayList<CrewMember> crewMembers;
	private JFrame frame = new JFrame();
	ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Create the application.
	 * 
	 * @param manager                  the all encompassing window manager
	 * @param crewMemebersToChooseFrom a list containing the crew members the user
	 *                                 can pick from
	 */
	public ScreenCrewMemberSelect(WindowManager manager, ArrayList<CrewMember> crewMemebersToChooseFrom) {
		this.crewMembers = crewMemebersToChooseFrom;
		startWindow(manager, frame);
	}

	private void handleSelected(int index) {
		CrewMember selected = crewMembers.get(index);

		manager.handleSelectedCrewMember((ScreenAbstractMenu) this, selected);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goBackToMainMenu();
			}
		});
		btnGoBack.setBounds(335, 227, 89, 23);
		frame.getContentPane().add(btnGoBack);

		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = buttonGroup.getSelection().getMnemonic();
				handleSelected(index);
			}

		});
		btnSelect.setBounds(10, 227, 89, 23);
		frame.getContentPane().add(btnSelect);

		/*
		 * Loop that iterates through crew members
		 * 
		 */

		for (int i = 0; i < crewMembers.size(); i++) {
			CrewMember crewMember = crewMembers.get(i);

			JRadioButton rdbtnTest = new JRadioButton(String.format("%s the %s (%d)", crewMember.getName(),
					crewMember.getType(), crewMember.getNumberOfFreeActions()));
			rdbtnTest.setBounds(35, 58 + 20 * i, 300, 23);
			rdbtnTest.setMnemonic(i);
			rdbtnTest.setForeground(Color.YELLOW);
			rdbtnTest.setBackground(Color.BLACK);
			rdbtnTest.setOpaque(true);
			if (i == 0) {
				rdbtnTest.setSelected(true);
			}
			frame.getContentPane().add(rdbtnTest);
			buttonGroup.add(rdbtnTest);
		}
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ScreenCrewMemberSelect.class.getResource("/img/mainBck.jpg")));
		lblNewLabel.setBounds(0, 0, 450, 278);
		frame.getContentPane().add(lblNewLabel);
	}
}
