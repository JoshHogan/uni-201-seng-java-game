package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import main.Game;

import java.awt.Color;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * The first setup screen that allows the user to choose the number of crew they
 * want, how many days the adventure will last and also the name of their ship
 */
public class ScreenSetupShip extends ScreenAbstractMenu {

	private JFrame frame = new JFrame();

	private JTextField shipNameInput;

	public ScreenSetupShip(WindowManager incomingManager) {
		startWindow(incomingManager, frame);
	}

	/**
	 * Closes this window and opens up the crew member select one
	 * 
	 * @param numberCrew the number of crew that the user chose
	 */
	public void finishedWindow(int numberCrew) {
		manager.closeShipSetupScreen(this, numberCrew);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel daysLbl = new JLabel("Please select the number of days you would like to travel (2-10)");
		daysLbl.setOpaque(true);
		daysLbl.setForeground(Color.YELLOW);
		daysLbl.setBackground(Color.BLACK);
		daysLbl.setHorizontalAlignment(SwingConstants.CENTER);
		daysLbl.setBounds(0, 26, 450, 22);
		frame.getContentPane().add(daysLbl);

		final JSlider daySldr = new JSlider();
		daySldr.setBackground(Color.BLACK);
		daySldr.setForeground(Color.YELLOW);
		daySldr.setPaintTicks(true);
		daySldr.setPaintLabels(true);
		daySldr.setMajorTickSpacing(1);
		daySldr.setToolTipText("");
		daySldr.setMinorTickSpacing(1);
		daySldr.setSnapToTicks(true);
		daySldr.setValue(6);
		daySldr.setMaximum(10);
		daySldr.setMinimum(3);
		daySldr.setBounds(65, 59, 322, 52);
		daySldr.setOpaque(true);
		frame.getContentPane().add(daySldr);

		JLabel shipNameLbl = new JLabel("Please enter the name of your new ship");
		shipNameLbl.setOpaque(true);
		shipNameLbl.setHorizontalAlignment(SwingConstants.CENTER);
		shipNameLbl.setForeground(Color.YELLOW);
		shipNameLbl.setBackground(Color.BLACK);
		shipNameLbl.setBounds(0, 123, 450, 22);
		frame.getContentPane().add(shipNameLbl);

		shipNameInput = new JTextField();
		shipNameInput.setHorizontalAlignment(SwingConstants.CENTER);
		shipNameInput.setBounds(145, 152, 150, 26);
		frame.getContentPane().add(shipNameInput);
		shipNameInput.setColumns(10);

		JLabel lblPleaseSelectNumber = new JLabel("Please select number of crew members");
		lblPleaseSelectNumber.setOpaque(true);
		lblPleaseSelectNumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblPleaseSelectNumber.setForeground(Color.YELLOW);
		lblPleaseSelectNumber.setBackground(Color.BLACK);
		lblPleaseSelectNumber.setBounds(0, 190, 450, 22);
		frame.getContentPane().add(lblPleaseSelectNumber);

		final JSlider crewNumSldr = new JSlider();
		crewNumSldr.setOpaque(true);
		crewNumSldr.setBackground(Color.BLACK);
		crewNumSldr.setForeground(Color.YELLOW);
		crewNumSldr.setValue(3);
		crewNumSldr.setToolTipText("");
		crewNumSldr.setSnapToTicks(true);
		crewNumSldr.setPaintTicks(true);
		crewNumSldr.setPaintLabels(true);
		crewNumSldr.setMinorTickSpacing(1);
		crewNumSldr.setMinimum(2);
		crewNumSldr.setMaximum(4);
		crewNumSldr.setMajorTickSpacing(1);
		crewNumSldr.setBounds(125, 218, 190, 52);
		frame.getContentPane().add(crewNumSldr);

		JButton nextBtn = new JButton("NEXT");
		nextBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// when the 'NEXT' button is pressed, days and ship's name are taken and passed
				// through game class
				manager.game = new Game(daySldr.getValue(), shipNameInput.getText());
				finishedWindow(crewNumSldr.getValue());
			}
		});
		nextBtn.setForeground(Color.BLACK);
		nextBtn.setBackground(Color.LIGHT_GRAY);
		nextBtn.setBounds(367, 243, 77, 29);
		frame.getContentPane().add(nextBtn);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ScreenSetupShip.class.getResource("/img/mainBck.jpg")));
		label.setBounds(0, 0, 450, 278);
		frame.getContentPane().add(label);

	}
}
