package gui;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;

/**
 * This is a menu that allows the user to select various actions that relate to
 * the ship, such as piloting and repairing it
 */
public class ScreenShipMenu extends ScreenAbstractMenu {

	private JProgressBar progressBar;
	private JFrame frame = new JFrame();

	/**
	 * Create the application.
	 * 
	 * @param manager the all encompassing window manager
	 */
	public ScreenShipMenu(WindowManager manager) {
		startWindow(manager, frame);

		progressBar.setValue(manager.game.ship.getShieldHealth());
		progressBar.setToolTipText(Integer.toString(progressBar.getValue()));
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void initialize() {
		frame.setResizable(false);
		frame.setBounds(100, 100, 467, 329);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnRepairShip = new JButton("Repair Ship");
		btnRepairShip.setEnabled(manager.game.crew.freeCrewMembers() >= 1);
		btnRepairShip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Close this window and Open the crew member select menu
				closeWindow();
				manager.repairShip();
			}
		});
		btnRepairShip.setBounds(53, 242, 140, 23);
		frame.getContentPane().add(btnRepairShip);

		JButton btnPilotShip = new JButton("Pilot Ship");
		btnPilotShip.setEnabled(manager.game.crew.freeCrewMembers() >= 2);
		btnPilotShip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Open the crew member select menu twice, used to pilot the ship
				closeWindow();
				manager.pilotShip();
			}
		});
		btnPilotShip.setToolTipText("You need two crew members free to pilot the ship");
		btnPilotShip.setBounds(205, 242, 114, 23);
		frame.getContentPane().add(btnPilotShip);

		JButton btnGoBack = new JButton("Go Back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Go back to main menu
				goBackToMainMenu();
			}
		});
		btnGoBack.setBounds(331, 242, 89, 23);
		frame.getContentPane().add(btnGoBack);

		progressBar = new JProgressBar();
		progressBar.setForeground(Color.GREEN);
		progressBar.setValue(20);
		progressBar.setBounds(68, 199, 330, 14);
		frame.getContentPane().add(progressBar);

		JLabel lblShieldHealth = new JLabel("Shield Health:");
		lblShieldHealth.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblShieldHealth.setForeground(Color.YELLOW);
		lblShieldHealth.setHorizontalAlignment(SwingConstants.CENTER);

		lblShieldHealth.setBounds(0, 158, 466, 31);
		frame.getContentPane().add(lblShieldHealth);

		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(ScreenShipMenu.class.getResource("/img/shield.png")));
		label_1.setBounds(162, 6, 157, 151);
		frame.getContentPane().add(label_1);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ScreenShipMenu.class.getResource("/img/mainBck.jpg")));
		label.setBounds(0, -12, 466, 319);
		frame.getContentPane().add(label);
	}
}
