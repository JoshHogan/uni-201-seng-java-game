package commandLine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import main.CrewMember;
import main.FoodItem;
import main.Game;
import main.Item;
import main.MedicalItem;
import main.TransporterPart;
import main.CrewMember.CrewMemberTypes;

/**
 * @author Josh Hogan
 *
 */
public class CommandLine {
	/**
	 * An input stream that allows reading from the console
	 */
	static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

	/**
	 * Does nothing as all functionality is all in main.
	 */
	public CommandLine() {
	}

	/**
	 * Prints a prompt, then reads an integer from the console, that must be between
	 * the given maximum and minimum values
	 * 
	 * @param prompt  The prompt string that is printed before waiting for user
	 *                input
	 * @param minimum The minimum value that will be accepted from the user
	 * @param maximum The maximum value that will be accepted from the user
	 * @return Returns the integer that the user typed into the console
	 */
	public static int readInt(String prompt, int minimum, int maximum) {
		int temp = -1;
		prompt = prompt + " (" + minimum + "-" + maximum + ")";
		while (temp > maximum || temp < minimum) {
			try {
				temp = Integer.parseInt(readLine(prompt));
			} catch (Exception e) {
				println("Please enter only a number");
			}
		}
		return temp;
	}

	/**
	 * Prints a prompt and reads a line from the console and returns it as a string
	 * 
	 * @param prompt The prompt string that is printed before waiting for user input
	 * @return the string that the user entered
	 */
	public static String readLine(String prompt) {
		print(prompt + " > ");
		return readLine();
	}

	/**
	 * Reads a line from the input stream
	 * 
	 * @return The line read or a blank string if an error occurred
	 */
	public static String readLine() {
		try {
			return input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * A wrapper function for System.out.print()
	 * 
	 * @param o The object to print
	 */
	public static void print(Object o) {
		System.out.print(o);
	}

	/**
	 * A wrapper function for System.out.println()
	 * 
	 * @param o The object to print
	 */
	public static void println(Object o) {
		System.out.println(o);
	}

	/**
	 * Allows the user to choose from a number of choices using the default prompt
	 * of "Which option would you like?"
	 * 
	 * @param choices the options the user has to choose from
	 * @return the number of the chosen choice, starting from 1
	 */
	public static int userMultiChoice(ArrayList<String> choices) {
		return userMultiChoice(choices, "Which option would you like?");
	}

	/**
	 * Allows the user to choose from a number of choices with a prompt
	 * 
	 * @param choices the options the user has to choose from
	 * @param prompt  the prompt question that is output at the end of the choices
	 * @return the number of the chosen choice, starting from 1
	 */
	public static int userMultiChoice(ArrayList<String> choices, String prompt) {
		for (int i = 1; i <= choices.size(); i++) {
			println(i + ": " + choices.get(i - 1));
		}
		return readInt(prompt, 1, choices.size());
	}

	/**
	 * Prints a starry break to the console to differentiate between different
	 * sections
	 */
	public static void printStars() {
		println("**********************************************************************");
	}

	/**
	 * The current game object containing all elements of the game
	 */
	static Game game;

	/**
	 * An Array-list to hold temporary options for the multiple-choice questions
	 */
	static ArrayList<String> tempChoices = new ArrayList<String>();

	/**
	 * The main function that runs the game
	 * 
	 * @param args the cmdline args (not used)
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// Print an introduction message
		println("Let's go");
		println("Welcome to CommandLine Space Explorer");

		// This is to speed up debug process, use custom environment
		if (/* readInt("Use pre built testing environment", 0, 1) */0 == 1) {
			game = new Game(10, "Coolio shipo");
			game.addCrewMemeber("Crew Member Uno", CrewMemberTypes.Pilot);
			game.addCrewMemeber("Crew member Dos", CrewMemberTypes.Explorer);
			game.addCrewMemeber("Crew member 3", CrewMemberTypes.ExtraHealth);
		} else {
			// Allow the user to input the number of days and the name of the ship
			int days = readInt("Enter the amount of Days that you would like to play", 3, 10);
			String shipName = readLine("Enter a name for your Ship");
			printStars(); // print a break between sections

			// Make a new game object
			game = new Game(days, shipName);

			// Allow the user to pick the number of crew members and give each one a name
			// and a type
			int numberOfCrew = readInt("Enter the number of Crew Members that you would like", 2, 4);
			for (int i = 1; i <= numberOfCrew; i++) {
				String name = readLine("Enter the name of Crew Member " + i);
				int type = readInt("Enter the type of Crew Member " + i, 1, 6);
				// Add each member to the crew, with zero indexing
				game.addCrewMemeber(name, CrewMemberTypes.values()[type - 1]);
				printStars();
			}
		}
		// Print more info
		println("You are on a desolate plant called " + game.planet.getName() + " ..blah blah\n");
		// Enter into the main game loop, runs until the game is complete
		while (game.gameComplete() == false) {
			printGeneralInfo();
			println("");// Print a break between sections
			// Ask the main multiple choice question for the main menu
			tempChoices.clear();
			tempChoices.add("Select a crew member to see status and avaliable tasks");
			tempChoices.add("Pilot or view the status of the ship");
			tempChoices.add("Visit the Space Outpost to buy items");
			tempChoices.add("Move to the next day");

			switch (userMultiChoice(tempChoices)) {
			case 1: // crew member select
				CrewMemberSelectMenu();
				break;
			case 2: // Ship information and piloting
				ShipMenu();
				break;
			case 3: // Buy items at the space outpost
				VisitSpaceOutpostMenu();
				break;
			case 4: // move to the next day
				MoveToNextDayMenu();
			}
		}
		// Game has now ended decide if win or lost
		printStars();
		printStars();
		printStars();
		println("");
		println("Game is complete");
		if (game.getPartsFound() >= game.getNumberOfParts()) {
			println("You win in only: " + game.getCurrentDay() + " out of " + game.getNumberOfDays() + " days");
		} else {
			println("You lost after finding only: " + game.getPartsFound() + " out of " + game.getNumberOfParts()
					+ " parts");
		}
	}

	/**
	 * This prints the general information about the crew and the ship so the user
	 * can have all the statistics quickly
	 */
	private static void printGeneralInfo() {
		printStars();
		println("+--------------------------------------------------------------------+");
		println("|                     Crew and Ship Information                      |");
		String format = "| %-30s | %-6d | %-6d | %-5d | %-7d |";
		println("+--------------------------------+--------+--------+-------+---------+");
		println("+           Name (Type)          | Health | Hunger | Tired | Actions |");
		println("+--------------------------------+--------+--------+-------+---------+");
		for (int i = 0; i < game.crew.numberOfCrew(); i++) {
			CrewMember selected = game.crew.getCrewMember(i);
			println(String.format(format, selected.getName() + " (" + selected.getType() + ")", selected.health,
					selected.hunger, selected.tiredness, selected.getNumberOfFreeActions()));
		}
		println("+--------------------------------+--------+--------+-------+---------+");
		println(String.format("| %-30s | %-6d | %26s", game.ship.getName(), game.ship.getShieldHealth(), "|"));
		println("+--------------------------------+--------+--------------------------+");
		println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
		println("+--------------------------------------------------------------------+");
		println("|                          Game Information                          |");
		println("+-------------+------------+-----++-------------+--------------+-----+");
		println("| Current Day | Total Days |  %  || Parts Found | Parts Needed |  %  |");
		println("+-------------+------------+-----++-------------+--------------+-----+");
		format = "| %-11d | %-10d | %-3d || %-11d | %-12d | %-3d |";
		int percentDays = game.getCurrentDay() * 100 / game.getNumberOfDays();
		int percentParts = game.getPartsFound() * 100 / game.getNumberOfParts();
		println(String.format(format, game.getCurrentDay(), game.getNumberOfDays(), percentDays, game.getPartsFound(),
				game.getNumberOfParts(), percentParts));
		println("+-------------+------------+-----++-------------+--------------+-----+");
	}

	/**
	 * The menu that allows the user to select a crew member, see information about
	 * them and select an action for them
	 */
	private static void CrewMemberSelectMenu() {
		printStars(); // Print a break between sections

		// Add all of the crew members to the multiple choice list
		tempChoices.clear();
		for (int i = 0; i < game.crew.numberOfCrew(); i++) {
			CrewMember current = game.crew.getCrewMember(i);
			// Show their name and number of free actions
			tempChoices.add(current.getName() + " (" + current.getNumberOfFreeActions() + ")");
		}
		// Allow the user to select one of the crew members
		int selection = userMultiChoice(tempChoices);
		CrewMember selected = game.crew.getCrewMember(selection - 1);

		// Print some information about the crew member
		printStars();
		println("Name: " + selected.getName());
		println("Health: " + selected.health);
		println("Tiredness: " + selected.tiredness);
		println("Hunger: " + selected.hunger);
		println("Free actions left: " + selected.getNumberOfFreeActions());
		println("");

		// Print the action menu
		tempChoices.clear();
		tempChoices.add("Go back to menu");
		if (selected.freeAction()) {
			tempChoices.add("Use or Eat an item from inventory");
			tempChoices.add("Sleep");
			tempChoices.add("Repair the shields of the ship");
			tempChoices.add("Explore the planet");
		}
		switch (userMultiChoice(tempChoices)) {
		case 1: // go back to main menu
			return;
		case 2: // use or eat an item
			printStars();
			// Check if the inventory of the crew is empty...
			if (game.crew.getInventory().size() == 0) {
				println("The inventory of your crew is empty");
				break;
			} else { // ...if not then print all items
				println("The items in the inventory of your crew...");
				tempChoices.clear();
				tempChoices.add("Go back to menu");
				for (Item item : game.crew.getInventory()) {
					tempChoices.add(item.toString());
				}
				int choice = userMultiChoice(tempChoices);
				if (choice == 1) { // They picked menu
					break;
				}

				choice = choice - 2; // zero indexed choices
				Item chosenItem = game.crew.getInventory().get(choice);

				// Use the item that the user picked for the selected crewMember
				if (chosenItem instanceof TransporterPart) {
					println("You cannot consumre a transporter part you egg");
				} else if (chosenItem instanceof FoodItem) {
					println(selected.eat((FoodItem) chosenItem));
				} else if (chosenItem instanceof MedicalItem) {
					println(selected.heal((MedicalItem) chosenItem));
				}
			}
			break;
		case 3: // sleep
			printStars();
			println(selected.sleep());
			break;
		case 4: // repair the ship
			printStars();
			println(selected.repairShip());
			break;
		case 5: // explore the planet
			printStars();
			println(selected.explorePlanet());
		default:
			break;
		}
		// Print a little bit of info about crew member again
		println("Name: " + selected.getName());
		println("Health: " + selected.health);
		println("Tiredness: " + selected.tiredness);
		println("Hunger: " + selected.hunger);
		println("Free actions left: " + selected.getNumberOfFreeActions());
	}

	/**
	 * This menu allows the user to see info about the ship and also select people
	 * to pilot it
	 */
	private static void ShipMenu() {
		// Print some information about the ship etc
		printStars();
		println("Ship Name: " + game.ship.getName());
		println("Shield Health: " + game.ship.getShieldHealth());
		println("");
		println("Free Crew Members: " + game.crew.freeCrewMembers());

		// Choose an action
		tempChoices.clear();
		tempChoices.add("Go back to menu");
		tempChoices.add("Repair Ship");
		tempChoices.add("Pilot Ship (needs two crew members)");
		switch (userMultiChoice(tempChoices)) {
		case 1: // go back to menu
			return;
		case 2: // repair ship
			printStars();
			// Check there is enough free crew members
			if (game.crew.freeCrewMembers() < 1) {
				println("Not enough free crew members");
			} else {
				CrewMember selected;
				// If there is exactly one crew member free then select them
				if (game.crew.freeCrewMembers() == 1) {
					selected = game.crew.getFreeCrewMembers().get(0);
					println("Selected " + selected.getName() + " as was only crew member free");
				} else { // otherwise let user pick a crew member
					// Add only the free crew members to the list of choices
					tempChoices.clear();
					for (int i = 0; i < game.crew.freeCrewMembers(); i++) {
						tempChoices.add(game.crew.getFreeCrewMembers().get(i).getName());
					}
					// Allow the user to select one of the crew members
					int selection = userMultiChoice(tempChoices);
					selected = game.crew.getFreeCrewMembers().get(selection - 1);
				}
				// Carry out the repair action
				println(selected.repairShip());
			}
			break;
		case 3: // pilot ship
			printStars();
			// First check there is enough free crew members
			if (game.crew.freeCrewMembers() < 2) {
				println("Not enough free crew members");
			} else if (game.ship.getShieldHealth() < 40) {
				// The shields aren't strong enough for the ship to fly
				println("Ship is not in a state to fly, please repair it");
			} else {
				CrewMember selected1;
				CrewMember selected2 = null;

				// If there is only two free then select them both
				if (game.crew.freeCrewMembers() == 2) {
					selected1 = game.crew.getFreeCrewMembers().get(0);
					selected2 = game.crew.getFreeCrewMembers().get(1);
					println(selected1.getName() + " and " + selected2.getName()
							+ " were chosen as were only free crew members");
				} else {
					println("Please Choose First Crew Member");
					// Add free crew members to multiple choice list
					tempChoices.clear();
					for (int i = 0; i < game.crew.freeCrewMembers(); i++) {
						tempChoices.add(game.crew.getFreeCrewMembers().get(i).getName());
					}
					// Allow the user to select one of the crew members
					int selection1 = userMultiChoice(tempChoices);
					selected1 = game.crew.getFreeCrewMembers().get(selection1 - 1);
					while (selected2 == null) {
						printStars();
						println("Please Choose Second Crew Member");
						// Let user only choose between free crew members
						tempChoices.clear();
						for (int i = 0; i < game.crew.freeCrewMembers(); i++) {
							tempChoices.add(game.crew.getFreeCrewMembers().get(i).getName());
						}
						// Allow the user to select one of the crew members
						int selection2 = userMultiChoice(tempChoices);
						// Check that the same member was chosen twice
						if (selection2 != selection1)
							selected2 = game.crew.getFreeCrewMembers().get(selection2 - 1);
						else
							println("Please don't select the same crew member");
					}
				}
				printStars();
				println(game.ship.pilot(selected1, selected2));
			}
			break;
		default:
			break;
		}
	}

	/**
	 * This menu allows the user to buy items at the space outpost
	 */
	private static void VisitSpaceOutpostMenu() {
		while (true) { // Allows the user to buy multiple items
			// Print some info about the users bank balance
			printStars();
			println("Bank Balance: " + game.crew.bankAccount);
			println("");

			// Check if the inventory of the crew is empty...
			if (game.crew.getInventory().size() == 0) {
				println("The inventory of your crew is empty");
			} else { // ...if not then print what is in it
				println("The inventory of your crew...");
				for (Item item : game.crew.getInventory()) {
					println(item);
				}
			}

			// Now print the items that the space outpost is selling
			println("");
			println("The Space Outpost is selling");
			tempChoices.clear();
			tempChoices.add("Go back to menu");
			for (Item item : game.spaceOutpost.getInventory()) {
				tempChoices.add("$" + item.price() + "\t" + item.toString());
			}
			// Allow the user to pick one
			int choice = userMultiChoice(tempChoices);
			if (choice == 1) { // They picked main menu
				return;
			}
			choice = choice - 2; // zero indexed choices
			// Checks the user has enough money and buys the item
			if (game.spaceOutpost.buy(choice)) {
				println("Purchase Successful");
			} else {
				println("Purchase Failed: your crew is too poor");
				return;
			}
		}
	}

	/**
	 * This moves the game on to the next day, making the user confirm if there are
	 * still free actions left for some crew members
	 */
	private static void MoveToNextDayMenu() {
		printStars();
		// If no actions are left then no confirmation needed
		if (game.crew.freeCrewMembers() == 0) {
			println(game.moveToNextDay());
		} else {
			// Make the user confirm they still want to move to next day
			println("They are still crew members with free actions");
			tempChoices.clear();
			tempChoices.add("Go back to menu");
			tempChoices.add("Continue to next day anyway");
			switch (userMultiChoice(tempChoices)) {
			case 1:
				return;
			case 2:
				printStars();
				println(game.moveToNextDay());
			}
		}
	}

}
